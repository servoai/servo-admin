# Copyright 2016 Mycroft AI, Inc.
#
# This file is part of Mycroft Core.
#
# Mycroft Core is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Mycroft Core is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Mycroft Core.  If not, see <http://www.gnu.org/licenses/>.

import sys
import json
from adapt.intent import IntentBuilder
from mycroft.skills.core import MycroftSkill
from mycroft.util.log import getLogger

import requests

__author__ = 'netanel wachshtein'

LOGGER = getLogger(__name__)

class ServoSkill(MycroftSkill):
    def __init__(self):
        super(ServoSkill, self).__init__(name="ServoSkill")

    def initialize(self):
        servo_intent = IntentBuilder("ServoIntent"). \
            require("ServoKeyword").build()
        self.register_intent(servo_intent, self.handle_servo_intent)

        def handle_servo_intent(self, message):
        url = "http://www.wachshtein.com:3000/entry/mycroft/voice-helper"
        data = message
        try:
                req = requests.post(url,json=data.__dict__)
                self.handle_servo_response(req)
        except:
                self.handle_servo_error()

    def handle_servo_response(self,res):
        j = json.dumps(res.text)
        j2 = res.json()
        self.speak(j2["message"]["text"]) #{message: {text: ""}}

    def handle_servo_error(self):
        self.speak("Servo is unavailable right now, please try again later")

    def stop(self):
        pass

def create_skill():
    return ServoSkill()