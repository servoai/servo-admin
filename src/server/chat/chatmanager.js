var _ = require('underscore');
var msbot = {}; //require("./msbot");
var facebookChatDriver = require('./facebook.v2').getInst();
var config = require("../config");
var mycroftService, chatsim;
var alexaService = require('./alexa.v2').getInst();
var utils = require("utils/utils");
var dblogger = require("utils/dblogger");
var fsmEventEmitter = require('FSM/fsm-event-emitter.js');
var ticker = require('FSM/ticker').getInst();


/**
 * Router for sending messages out to different channels
 * {
        recipient: { id: 1 },
        sender: { id: 2 },
        text: "",
            treeID: TESTPID,
                raw: { }
    }
 */
class ChatManager {
  static createMessageObject(channel, dataObj) {

    switch (channel) {
      case "chatsim":
        chatsim = require('./chatsim').getInst();
        return chatsim.createMessageObject(
          dataObj.recipient,
          dataObj.sender,
          dataObj.text,
          dataObj.treeID,
          dataObj.raw);
    }
  }
  /***
   * send a message depending on the context
   */
  static sendMessage(prompt, tree, process, node) {

    var processData = process.get('data', tree.id);
    var message = prompt.text || prompt;
    var customer = process.customer;
    if (utils.isEmpty(message) && utils.isEmpty(prompt.payload || prompt.view)) {

      return new Promise(function (resolve, reject) {
        reject("empty image,view and text. nothing sent");
      });
    }
    // statistics
    process.data('lastMessage', Date.now());
    ticker.start(process.id);

    dblogger.flow(' sending message ' + (prompt.text || ""), prompt);
    if (customer && (customer.fbId || customer.channel === "facebook")) {
      return new Promise(function (resolve, reject) {
        var fsmModel = require('models/fsmmodel');
        var fsm = fsmModel.getFSMSync(process.fsm_id);
        if (!fsm) {
          dblogger.log('WARNING: no fsm ', process.fsm_id);
        }
        facebookChatDriver.sendMessage(prompt, customer.fbId || customer.id, fsm, node).then((postObj) => {

          // save the state
          process.save().then(() => {
            resolve(postObj);
            fsmEventEmitter.messageSent(postObj, process);
          }).catch((err) => {
            dblogger.error(err);
          });
        }, function (err) {
          dblogger.error('SENDMESSAGE ERROR:---->', process.summary());
          reject(err);
        });
      });
    } else if (process.messengerType && process.messengerType.toUpperCase() === "MSBOT") {
      // not working
      return msbot.sendMessage(process.session, message, processData.images);
    } else if (process.properties() && process.properties().channels && process.properties().channels.indexOf("mycroft") != -1) {

      return new Promise(function (resolve, reject) {
        mycroftService = mycroftService || require('./mycroft').getInst();
        mycroftService.sendMessage(prompt, customer.id, tree, node).then((postObj) => {
          // save the state
          process.save().then(() => {
            resolve(postObj);

            fsmEventEmitter.messageSent(postObj, process);
          }).catch((err) => {
            dblogger.error(err);
          });
        }, function (err) {
          reject(err);
        });
      });
    } else if (process.properties() && process.properties().channels &&
      (process.properties().channels.toLowerCase().indexOf("chatsim") != -1)) {
      return new Promise((resolve) => {
        chatsim = require('./chatsim').getInst();
        chatsim.sendMessage(prompt, process.id, tree, node).then((postObj) => {

          // save the state
          process.save().then(() => {
            resolve(postObj);

            fsmEventEmitter.messageSent(postObj, process);
          });
        }, function (err) {
          resolve(err);
        }).catch((err) => {
          dblogger.error(err);
          resolve(err);
        });
      });
    } else if (process.properties() && process.properties().channels &&
      (process.properties().channels.toLowerCase().indexOf("websocket") != -1)) {
      return new Promise(function (resolve) {
        let websocket = require('./websocket-driver').getInst();
        websocket.sendMessage(prompt, process.id, tree, node).then((postObj) => {

          // save the state
          process.save().then(() => {
            resolve(postObj);

            fsmEventEmitter.messageSent(postObj, process);
          });
        }, function (err) {
          resolve(err);
        }).catch((err) => {
          dblogger.error(err);
          resolve(err);
        });
      });
    }
    if (process.properties() && process.properties().channels && process.properties().channels.indexOf('alexa') > -1) {
      return new Promise(function (resolve, reject) {

        alexaService.sendMessage(prompt, process, tree, node).then((postObj) => {

          // save the state
          process.save().then(() => {
            resolve(postObj);
            fsmEventEmitter.messageSent(postObj, process);

          }).catch((err) => {
            dblogger.error(err);
          });


        }, function (err) {
          reject(err);
        });
      });
    } else {
      dblogger.error("no channel detected", process.id, node.id, tree.id);
      return new Promise((resolve) => {
        resolve("no channel detected" + process.id + "/" + node.id + "/" + tree.id);
      });
    }
  }

  /**
   * start all fsms
   */
  static startAll(app, fsms) {

    facebookChatDriver.startAll(app, fsms);

    alexaService.startAll(app, fsms);

    // avoid circular ref
    mycroftService = mycroftService || require('./mycroft').getInst();
    mycroftService.startAll(app, fsms);

    // avoid circular ref
    chatsim = chatsim || require('./chatsim').getInst();
    chatsim.startAll(app, fsms);
  }

  /**
   * stop all
   * @param {*} app - the express app
   */
  static stopAll(app) {

    facebookChatDriver.stopAll();

    alexaService.stopAll();

    // avoid circular ref
    mycroftService = require('./mycroft').getInst();
    mycroftService.stopAll();

    // avoid circular ref
    chatsim = require('./chatsim').getInst();
    chatsim.stopAll(app);

  }
}



module.exports = ChatManager;
