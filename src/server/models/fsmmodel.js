const Promise = require('bluebird');
var utils = require("../utils/utils");
var dblogger = require("../utils/dblogger.js");
var _ = require('underscore');
var baseModel = require("./base-model");
const fs = Promise.promisifyAll(require('fs-extra'));
/*const glob = require("glob-fs")({
  gitignore: true
});*/
const glob = require('glob');
const pathLib = require('path');

function fsmModel() {}
_.extend(fsmModel, baseModel);

const CONVOCODE_DIR = "./convocode";

var _fsms;
module.exports = fsmModel;

/***
 * get by id
 * @param fsm_id
 * @return {*}
 */
fsmModel.get = function (fsm_id) {
  return fsmModel.getFSM(fsm_id);
};

/**
 * get an fsm by id synchrnously
 * @param {*} fsm_id 
 */
fsmModel.getFSMSync = function (fsm_id) {
  return _fsms[fsm_id];
};

/***
 * get by id
 * @param fsm_id
 * @return {*}
 */
fsmModel.getFSM = function (fsm_id) {
  return new Promise(function (resolve, reject) {
    fsmModel.getAllFSMs().then(function () {
      var resultFSM = _fsms[fsm_id];
      if (!resultFSM)
        reject({
          errorFsmId: fsm_id,
          message: "FSM " + fsm_id + " not found!"
        });
      else {
        resolve(resultFSM);
      }
    }).catch((ex) => {
      dblogger.error(ex);
    });
  });
};

function finalizeFsmLoad(fsms, errors) {
  const fsmCount = Object.keys(fsms).length;
  dblogger.log(`getAllFSMs() - successfully read: ${fsmCount} out of ${fsmCount + errors.length} trees`);
  if (errors.length != 0) {
    dblogger.error("FSMModel/getAllFSMs() - error loading FSM files:", errors);
    // We do want to continue to support all valid existing FSMs.
  }
  // save in memory cache
  _fsms = fsms;
}

const getModifiedDate = (path) => {
  if (fs.existsSync(path)) {
    const stats = fs.statSync(path);
    return stats.mtime;
  } else {
    return "Never";
  }
};

const readFSM = (path) => {
  // TODO: change to promise use
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        try {
          let json = JSON.parse(data);
          resolve(json);
        } catch (err) {
          reject(err);
        }
      }
    });
  });
};

/**
 * service function - add the fsms
 */
function addFsms(resFsms) {
  _fsms = _fsms || {};
  // add one by one to global tree cache
  for (var i = 0; i < resFsms.length; i++) {
    var fsm = resFsms[i];
    // tree could exist already - 
    // 2 trees can use the same subtree, defined in each
    utils.safeAdd(_fsms, fsm.id, fsm);
  }
}

/**
 * add an fsm to the cache
 * @param {*} fsm 
 * @return a promise
 */
fsmModel.addFsm = function (fsm) {
  addFsms([fsm]);
};

const normalizePath = (path) => {

  let re = new RegExp("\\" + pathLib.win32.sep, 'g');
  let normalized = path.replace(re, pathLib.posix.sep);
  if (normalized.indexOf(":") == 1) {
    normalized = normalized.substring(2);
  }
  return normalized;
};

/***
 * get all FSMs.
 * The assumption is the user has a folder named fsms,
 * under it there are ONLY subdirectories which named after FSM ID,
 * under each one there is a filed named after the FSM ID with a fsm.js extension.
 * If folder named with .deleted - it won't load it's FSM.
 * //@rfturn {Promise}
 */
fsmModel.getAllFSMs = function (userId, loadDrafts = false, rootOnly = false) {
  const excludeList = ["deleted", "actions", "conditions", "views", "brook", "common"];
  excludeList.forEach(item => `/${item}/`);
  excludeList.push(".DS", "settings.json");

  const hasExcluded = path => excludeList.reduce((has, item) => has || path.indexOf(item) !== -1, false);

  return new Promise(function (resolve, reject) {
    if (_fsms && !loadDrafts && !rootOnly) {
      return resolve(_fsms);
    }

    _fsms = {};
    // TODO1: glob search under userId/fsms/
    glob(CONVOCODE_DIR + '/**/*.json', function (er, files) {
      if (er) {
        dblogger.error('error in reading ' + CONVOCODE_DIR, er);
        reject(er);
      } else {
        const sorted = files.sort((a, b) => a.localeCompare(b));
        let filtered = sorted.filter(file => !hasExcluded(file));
        filtered = filtered.filter(file => file.includes(".json"));
        const errors = [];
        dblogger.log(`getAllFSMs() - reading: ${filtered.length} trees`);
        filtered.forEach((file, i) => {

          let path = file;
          path = path.substring(path.indexOf(CONVOCODE_DIR.substring(2)));
          path = normalizePath(path);
          const sections = path.split("/");
          const fsmInfo = {};
          fsmInfo.path = path;
          fsmInfo.userId = sections[0];
          fsmInfo.isDraft = sections[1] == "drafts";
          fsmInfo.isRoot = sections.length == 5;
          fsmInfo.folderName = pathLib.dirname(fsmInfo.path);

          readFSM(file).then(fsm => {
            fsmInfo.lastSaved = fsmInfo.lastPublished = getModifiedDate(file);
            if (fsmInfo.isDraft) {
              const publishedPath = file.replace(`${pathLib.posix.sep}drafts${pathLib.posix.sep}`, `${pathLib.posix.sep}fsms${pathLib.posix.sep}`);
              fsmInfo.lastPublished = getModifiedDate(publishedPath);
            } else {
              const draftPath = file.replace(`${pathLib.posix.sep}fsms${pathLib.posix.sep}`, `${pathLib.posix.sep}drafts${pathLib.posix.sep}`);
              fsmInfo.lastSaved = getModifiedDate(draftPath);
            }
            Object.assign(fsm, fsmInfo);
            fsmModel.addFsm(fsm);
            if (i == filtered.length - 1) {
              finalizeFsmLoad(_fsms, errors);
              resolve(_fsms);
            }
          }).catch(err => {
            errors.push(err + ": fsm:" + fsmInfo.path);
            if (i == filtered.length - 1) {
              finalizeFsmLoad(_fsms, errors);
              resolve(_fsms);
            }
          });
        });
      }
    });
  });
};

/**
 * purge the cache and reload FSMs
 */
fsmModel.reload = function () {
  _fsms = undefined;
  // reload and reset
  fsmModel.getAllFSMs().then((fsms) => {
    var chatManager = require('../chat/chatmanager');
    var express = require('express');

    var app = express();
    chatManager.startAll(app, fsms);
  });
  // TODO: RESET PROCESS NODE MEMORY (BUT NOT CONTEXT AND GLOBAL MEMORIES)
};
