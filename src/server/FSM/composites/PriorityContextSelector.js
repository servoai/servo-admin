var Tick = require('FSM/core/tick');
var b3 = require('FSM/core/b3');
var Composite = require('FSM/core/composite');
var _ = require('underscore');
var dblogger = require('utils/dblogger');
var statsManager = require('FSM/statsManager');
var ContextManager = require('FSM/contextManager');
/**
 * A key-value pair object where key is language code, pointing to a string 
 * @typedef TextObject
 */
/**
 * Selects by message intent & entities the children. A context is created for each child. 
 * Contexts may have an expected intentId (a string or a regex) and entities, or a helper:true that would occur as default, if no matching intent was found 
 * If execution has a context already, it will continue to tick the current child, unless a bottom-up context search 
 *  changes the selected child.
 * If no child is selected, Helper child is enetered every tick. 
 *  ContextSelector is closed When no child is running. the entities from last child are  mapped to the parent, and all contexts are cleared.
 * The target is mapped to the expected intents and entities.
 * If more than one entity of a certain name, an entity array will be created.
 * If leaveIfNoEntity is set, the previous context field will remain if currently there is no such entity.
 * If no child could be selected, a FAILURE is returned
 * @memberof module:Composites
 */
class PriorityContextSelector extends Composite {
  /**
   * ctor
   *  
   */
  constructor(settings = null) {
    settings = settings || {};

    super(settings);
    // this is a context node for the scoreres sub trees
    this.title = this.name = 'PriorityContextSelector';
    this.description = "Selects by message intent & entities the children. A context is created for each child. " +
      " Contexts may have an expected intentId (a string or a regex) and entities, or a helper:true that would occur as default, if no matching intent was found " +
      "If execution has a context already, it will continue to tick the current child, unless a bottom-up context search " +
      " changes the selected child." +
      "If no child is selected, Helper child is enetered every tick. " +
      " ContextSelector is closed When no child is running. the entities from last child are  mapped to the parent, and all contexts are cleared." +
      "The target is mapped to the expected intents and entities." +
      "If more than one entity of a certain name, an entity array will be created." +
      "If leaveIfNoEntity is set, the previous context field will remain if currently there is no such entity." +
      "If no child could be selected, a FAILURE is returned";
    /**
     * @typedef ContextItem
     * @prop {string} intentId
     * @prop {TextObject} description
     * @prop {boolean} helper
     */
    /**
     * Node parameters
     * @property parameters
     * @type {Object}
     * @property {contexts:Array<{ContextItem>,entities:Array<EntitiesToContextMapItem>} parameters.contexts
     */
    this.parameters = _.extend(this.parameters, {
      contexts: [{
        helper: false,
        intentId: '.*',
        description: {
          'en': 'Pass through everything'
        },
        entities: [{
          'contextFieldName': '',
          'entityName': '',
          'entityIndex': 0,
          'leaveIfNoEntity': true
        }]
      }]
      // ,
      // helpMessage: ['Sorry, didn\'t understand. Could you explain?', 'Hmmm... could you elaborate?']
    });

    this.contextManager = new ContextManager(this);


  }

  /**
   * after connection of children, add another for ticks with no target
   */
  postConnect() {

  }

  currentContextChild(tick) {
    var childIndex = this.local(tick, 'runningChild');
    return this.children[childIndex];
  }

  /**
   * get the properties of all contexts on the contextmanager node
   */
  contextProperties() {
    return this.properties.contexts;
  }

  contextChildren() {
    return this.children;
  }


  setContextChild(tick, selectedConvoIndex) {
    dblogger.assert(selectedConvoIndex < this.children.length, 'index too hight');
    dblogger.assert(selectedConvoIndex !== undefined, 'index undefined ' + this.title + "/" + this.id);
    this.local(tick, 'runningChild', selectedConvoIndex);
  }


  /**
   * map entities and tick down
   * 
   * The context can be searched only below a certain point in the tree. 
   * If the conversation is at a certain context, and the context is switched to a different context, then
   * that switch point is the place to start. otherwise, we dont have a context yet, so we search from the top 
   * 
   * @param {Tick} tick 
   * @return {void}
   * @private
   */
  tick(tick) {

    var child = this.local(tick, 'runningChild');
    // if not managed to select here 
    if (child < 0) {
      return tick.target.exists() ? b3.FAILURE() : b3.SUCCESS();
    }
    // TODO: MAP ENTITIES BEFORE AND AFTER?
    // otherwise go by the currently selected (either from open or from pre-tick)
    let status = this.contextManager.tick(tick);
    return status;
  }


  /**
   * selects the intent index based on target, if exists
   * @param {Tick} tick 
   */
  open(tick) {

    // if context does not needs selection, return 
    if (this.local(tick, 'runningChild') === undefined) {
      this.local(tick, 'runningChild', -1);
    }
    if (this.local(tick, 'runningChild') >= 0) {
      return;
    }

    dblogger.assert(!this.children[this.children.length - 1].properties.helper,
      "first child in a PriorityContextSelector must not be a helper");
    let foundContext = {
      index: -1
    };

    // initialize context frames
    this.contextManager.setContextFrames(tick, []);

    // no context got selected yet: then if we have a target, select by it the right context child
    // this answers cases where the first selection come from above (and tick is 'downwards')
    foundContext = this.contextManager.findContextToSelect(tick, "downwards");


    // initialize the running child
    this.local(tick, 'runningChild', foundContext.index);


    // only now you can open openContext 
    statsManager.openContext(tick, this);

    // set this as the context, on any case, when opened.
    this.contextManager.setCurrentContext(tick);

    // if we had a selection, treat it as a context switch, so we'll know to get back to it
    if (foundContext.index >= 0) {
      this.contextManager.switchContext(tick, foundContext.index);
    }


  }

  /**
   * close contexts if there's a real child ever opened
   * @param {Tick} tick 
   * @param {TickStatus} status 
   */
  close(tick, status) {

    // if context was not selected, return 
    this.contextManager.close(tick, status);
    statsManager.closeContext(tick, this);


  }

  /**
   * @return {Array<Validator>}
   */
  validators(node) {
    function findNone(node) {
      if (node.properties.contexts) {
        for (var i = 0; i < node.properties.contexts.length; i++) {
          if (node.properties.contexts[i].intentId && typeof node.properties.contexts[i].intentId === 'string' && node.properties.contexts[i].intentId.toLowerCase() === 'none') {
            return true;
          }
        }
      }
      return false;
    }

    function emptyContext(node) {
      if (node.properties.contexts) {
        for (var i = 0; i < node.properties.contexts.length; i++) {
          if (node.properties.contexts[i].helper) {
            continue;
          }
          if (!node.properties.contexts[i].intentId) {
            if (!node.properties.contexts[i].entities) {
              return true;
            }
            var nonEmptyEtt = false;
            for (var e = 0; e < node.properties.contexts[i].entities.length; e++) {
              if (node.properties.contexts[i].entities[e] && node.properties.contexts[i].entities[e].entityName && node.properties.contexts[i].entities[e].contextFieldName) {
                nonEmptyEtt = true;
              }
            }
            if (!nonEmptyEtt) {
              return true;
            }

          }
        }
      }
      return false;
    }
    return [{
        condition: node.children && node.children.length,
        text: "should have children"
      }, {
        condition: node.properties.contexts && node.properties.contexts.length,
        text: "should have at least 1 intent "
      },
      {
        condition: node.properties.contexts && (node.child || (node.children && node.properties.contexts.length === node.children.length)),
        text: "contexts number should be equal to number of children"
      },

      {
        condition: !findNone(node),
        text: "None is a reserved word and cannot be used as an intentId"
      },
      {
        condition: !emptyContext(node),
        text: "An empty or incomplete context exists"
      }
    ];
  }

}

module.exports = PriorityContextSelector;
