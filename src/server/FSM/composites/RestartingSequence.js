/**
 * Sequence
 *
 * Copyright (c) 2017 Servo Labs Inc.  
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 **/



// namespace:
var b3 = require('../core/b3')
var Composite = require('../core/composite')


/**
 * The Sequence node ticks its children sequentially until one of them returns 
 * `FAILURE`, `RUNNING` or `ERROR`. If all children return the success state, 
 * the sequence also returns `SUCCESS`. on every tick, it restarts from child 0
 *@memberof module:Composites
 **/
class RestartingSequence extends Composite {


  constructor() {
    super();
    this.title = this.name = 'RestartingSequence';
    this.description = "Runs all children synchronuously, always starting from first child. Use with caution";
  }

  /**
   * Tick method.
   *
   * @private tick
   * @param {Tick} tick A tick instance.
   * @return {Constant} A state constant.
   **/
  tick(tick) {
    for (var i = 0; i < this.children.length; i++) {
      var status = this.children[i]._execute(tick);
      // if tick never happened
      if (!status) {
        continue;
      }
      if (status !== b3.SUCCESS()) {
        return status;
      }
    }

    return status;
  }

  /**
   * @return {Array<Validator>}
   */
  validators(node) {
    return [{
      condition: node.children && node.children.length,
      text: "should have children"
    }];
  }
}
module.exports = RestartingSequence;
