var Tick = require('FSM/core/tick');
var b3 = require('FSM/core/b3');
var Composite = require('FSM/core/composite');
var _ = require('underscore');
var dblogger = require('utils/dblogger');
var fsmModel = require('models/fsmmodel');
var statsManager = require('FSM/statsManager');
var ContextManager = require('FSM/contextManager');
var uuid = require("uuid");
/**
 * Sends the message based on prompt or view properties. image is an html file name under images folder.
 * imageDataArrayName is the composite field name for an array object that contains data for the images.
 * Once sent, waits for a response and then directs the flow to the child found according to the intents/entities map.
 * Selects by message intent & entities the children. A context is created for each child. 
 * Contexts may have an expected intentId (a string or a regex) and entities, or a helper:true that would occur as default, if no matching intent was found 
 * If execution has a context already, it will continue to tick the current child, unless a bottom-up context search 
 *  changes the selected child.
 * If no child is selected, Helper child is entered every tick. 
 * On a timeout (user haven't answered in runningTimeoutSec) the timeout child is entered. If there's no timeout child, resort to default timeout behaviour (see BaseNode).
 * Node is closed when no more children are running. The entities from last child are  mapped to the parent, and all contexts are cleared.
 * The target is mapped to the expected intents and entities.
 * If more than one entity of a certain name, an entity array will be created.
 * If leaveIfNoEntity is set, the previous context field will remain if currently there is no such entity.
 * @memberof module:Composites
 */
class AskInContext extends Composite {

  constructor() {

    super();
    // this is a context node for the scoreres sub trees
    this.title = this.name = 'AskInContext';
    var parameters = {
      "view": false,
      "prompt": [],
      "imageHTML": false,
      "imageDataArrayName": ""

    };
    /**
     * Node parameters
     * @property parameters
     * @type {Object} parameters
     * @property {(ExpressionString|Object|Array<ExpressionString>|Array<TextObject>)} parameters.prompt - a textual message to the user. can contains an array for random messages. can contain an object with "language" keys.
     * @property {(ExpressionString|Object)} parameters.view - a file name of a view, or a view JSON object, to be used instead of the prompt in order to send native json
     * @property {ExpressionString} parameters.image - an html string or a file name, that is rendered as an image to send the user
     * @property {CompositeFieldName} parameters.imageDataArrayName - composite (message./global./context./volatile./local.) field name for an array object that contains data for the images
     **/
    this.parameters = _.extend(this.parameters, parameters);
    this.description = "Send the message based on prompt or view properties. image is an html file name under images folder." +
      " imageDataArrayName is the composite field name for an array object that contains data for the images"
    this.description += ". Once sent, waits for a response and then directs the flow to the child found according to the intents/entities map";
    this.parameters = _.extend(this.parameters, {
      replayActionOnReturnFromContextSwitch: true,
      intents: [{
        intentId: '',
        description: {
          'en': ''
        },
        entities: [{
          'contextFieldName': '',
          'entityName': '',
          'entityIndex': 0,
          'leaveIfNoEntity': true
        }]
      }]

    });

    this.contextManager = new ContextManager(this);


  }

  isQuestion() {
    return true;
  }

  /**
   *if we are on wakeup, and this is an opened leaf which is waiting for an answer
   * it could be:
   * 1. leaf was opened, client closed, client opened. if the leaf is AskAndWait, 
   * this means that the leaf was requesting answer that never came
   * so we need to re-play the node on the client upon wakeup
   * 2. leaf was opened on wakeup, asked the question, no ack yet, and now its second tick.
   * so we dont  need to replay upon wakeup
   *3. leaf was opened, server closed, server re-opened. client still SHOULD have an open
   *session, we upon server re-run we will come here, node isOpened, waitcode on running, 
   *waiting for an answer. This will NOT work for Alexa. TODO: solve that
   * @param {Tick} tick 
   * @private
   */
  enter(tick) {

    if (tick.target && tick.target.isWakeUp() &&
      this.get(tick, 'wokeupTargetId') !== tick.target.id() &&
      tick.process.get('isOpen', tick.tree.id, this.id)) {

      this._closeMe(tick);
    }

  }

  /**
   * open is called after the node RETURNS after RUNNING
   * @private
   */
  open(tick) {
    this.local(tick, 'step', 0);
    if (tick.target && tick.target.isWakeUp()) {
      this.set(tick, 'wokeupTargetId', tick.target.id());
    }

    dblogger.assert(this.children[this.children.length - 1] && !this.children[this.children.length - 1].properties.helper,
      "first child in a AskInContext must not be a helper");

    // default context
    this.local(tick, 'runningChild', -1);
    this.contextManager.setContextFrames(tick, []);
    // now you can open stat manager
    statsManager.openContext(tick, this);


  }

  /**
   * close
   * @param {*} tick 
   * @private
   */
  close(tick, status) {
    if (tick.target && tick.target.isWakeUp()) {
      this.set(tick, 'wokeupTargetId', undefined);
    }

    // if we ever got a selection
    if (this.currentContextChild(tick) != null) {
      // this will map entities to pARENT
      this.contextManager.close(tick, status);
      statsManager.closeContext(tick, this);
    }

    // TODO:send a message to stop listening (if driver is voice)

  }



  /**
   * get current context child
   * @param {*} tick 
   */
  currentContextChild(tick) {
    var childIndex = this.local(tick, 'runningChild');
    return this.children[childIndex];
  }

  /**
   * get the properties of all contexts on the contextmanager node
   */
  contextProperties() {
    return this.properties.intents;
  }

  /**
   * map entities and tick down
   * 
   * The context can be searched only below a certain point in the tree. 
   * If the conversation is at a certain context, and the context is switched to a different context, then
   * that switch point is the place to start. otherwise, we dont have a context yet, so we search from the top 
   * 
   * @param {Tick} tick 
   * @param {*} target 
   */
  tick(tick, target) {
    // var data = this.data(tick);
    var step = tick.process.get('step', tick.tree.id, this.id);
    // step 0: output message, then remove target
    if (step === 0) {
      // move to next step
      tick.process.set('step', 1, tick.tree.id, this.id);
      var msgOut = this.tickMessage(tick);
      if (msgOut === b3.SUCCESS()) {

        // continue to return RUNNING/FAILURE
        this.waitCode(tick, b3.RUNNING());
      } else if (msgOut !== b3.RUNNING()) {
        this.waitCode(tick, msgOut);
      }

      // remove this target, which led us here. this is not the target on which we need to find the context
      tick.target.remove();
    }

    // step 1: wait for new target
    if (step === 1) {
      // set ourselves
      this.contextManager.setCurrentContext(tick);
      // get the current child
      let child = this.local(tick, 'runningChild');
      // if hasnt found yet
      if (_.isUndefined(child) || child === null || child < 0) {

        // we have a target and no child
        if (tick.target.exists()) {
          // it means there was no child that could be found - neither intent-child nor helper
          // restart node so it would ask the question again
	  
          this._close(tick);

          //**  it means we got here from above - it means, we are with the right context for the anscestor context selector
          // the mapping happened already - for the context on which we are. 
          // if we have mappings here, it will need to get mapped to parent to be useful
          // so remark - this.contextManager.mapEntitiesToContext(tick, this.contextProperties());
          tick.target.remove();

        }
        return this.handleTimeout(tick);
      } else {

        var selected = this.contextChildren()[child];

        // map on current context
        this.contextManager.mapEntitiesToContext(tick, this.contextProperties(), child);

        // otherwise go by the currently selected (either from open or from pre-tick)
        let status = this.contextManager.tick(tick);

        // map after more specific contexts mapped
        this.contextManager.mapEntitiesToContext(tick, this.contextProperties(), child);
        return status;
      }

    }
    // if nothing found, something is wrong (if all children are context-ed, and this message couldnt find anyone) , return failure
    //dblogger.assert(false, 'nothing set')
    return this.waitCode(tick);

  }

  /**
   * selects a timeout child if exists
   * @param {Tick} tick 
   */
  handleTimeout(tick) {

    // check for timeout if we havent yet selected the child
    if (this.currentChildIndex(tick) < 0 && this.isNodeTimeout(tick)) {
      // see if we have a timeout child
      this.incrementRetries(tick);
      let timeoutChild = this.findTimeoutChild();
      // if not found, it will be dealt ON THE BASENODE
      if (timeoutChild > -1) {
        // other wise set it as the context child!
        this.setContextChild(tick, timeoutChild);

      }
    }
    return b3.RUNNING();

  }

  /**
   * find the first child that doesnt have any intent or entities
   */
  findTimeoutChild() {
    return this.contextProperties().findIndex((elem, index) => {
      return this.contextProperties()[index].timeout;
    });
  }


  /**
   * the context children are all the children
   * 
   */
  contextChildren() {
    return this.children;
  }


  setContextChild(tick, selectedConvoIndex) {
    dblogger.assert(selectedConvoIndex < this.children.length, 'index too hight');
    dblogger.assert(selectedConvoIndex !== undefined, 'index undefined at ' + this.title + "/" + this.id);
    this.local(tick, 'runningChild', selectedConvoIndex);
  }

  /**
   * @return {Array<Validator>}
   */
  validators(node) {
    function findNone(node) {
      if (node.properties.intents) {
        for (var i = 0; i < node.properties.intents.length; i++) {
          if (node.properties.intents[i].intentId && node.properties.intents[i].intentId.toLowerCase() === 'none') {
            return true;
          }
        }
      }
      return false;
    }

    function emptyContext(node) {
      if (node.properties.intents) {
        for (var i = 0; i < node.properties.intents.length; i++) {
          if (node.properties.intents[i].helper || node.properties.intents[i].timeout) {
            continue;
          }
          if (!node.properties.intents[i].intentId) {
            if (!node.properties.intents[i].entities) {
              return true;
            }
            var nonEmptyEtt = false;
            for (var e = 0; e < node.properties.intents[i].entities.length; e++) {
              if (node.properties.intents[i].entities[e] && node.properties.intents[i].entities[e].entityName && node.properties.intents[i].entities[e].contextFieldName) {
                nonEmptyEtt = true;
              }
            }
            if (!nonEmptyEtt) {
              return true;
            }

          }
        }
      }
      return false;
    }
    return [{
        condition: (node.properties.prompt && Object.keys(node.properties.prompt).length) ||
          (node.properties.view && Object.keys(node.properties.view).length),
        text: "either prompt or view should be non empty"
      }, {
        condition: node.children && node.children.length,
        text: "should have children"
      }, {
        condition: node.properties.intents && node.properties.intents.length,
        text: "should have at least 1 intent "
      },
      {
        condition: node.properties.intents && (node.child || (node.children && node.properties.intents.length === node.children.length)),
        text: "intents number should be equal to number of children"
      },
      {
        condition: node.properties.intents && node.children &&
          (node.properties.intents.length > 1 || (node.properties.intents.length == 1 && node.children.length === 1 && node.properties.intents[0].helper)),
        text: "a single intent must be set to be a helper"
      },
      {
        condition: (!((node.properties.prompt && Object.keys(node.properties.prompt).length) &&
          (node.properties.view && Object.keys(node.properties.view).length))),
        text: "Both prompt and view are non empty; view will be ignored"
      },

      {
        condition: !findNone(node),
        text: "None is a reserved word and cannot be used as an intentId"
      },
      {
        condition: !emptyContext(node),
        text: "An empty or incomplete context exists"
      }
    ];
  }
}

module.exports = AskInContext;
