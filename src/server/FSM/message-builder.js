var config = require("../config");
var viewModel = require("../models/view-model.js");
var htmlModel = require("../models/html-model.js");
var Promise = require('promise');
var dblogger = require("../utils/dblogger.js");
var _ = require('underscore');
var utils = require("../utils/utils");
var fsmModel = require('../models/fsmmodel');

function messageBuilder() {};
module.exports = messageBuilder;


/***
 * process the image/html/htmlImages entry
 */
function processImages(tick, node) {
  var process = tick.process;

  var folderName = fsmModel.getFSMSync(process.fsm_id).folderName;
  var imgPromise = new Promise(function (imgsResolve, imgsReject) {
    // take all the htmls and convert them to images
    var htmlImages = node.images();
    if (!htmlImages) {
      imgsResolve();
    } else {
      var data = node.alldata(tick);
      var htmls = Array.isArray(htmlImages) ? htmlImages : [{
        "status-0": htmlImages
      }];
      var imagesCount = htmls.length;

      _.each(htmls, function (htmlImage, i) {
        var arrOfPair = _.pairs(htmlImage);
        if (!arrOfPair[0]) {
          imgsReject("error at htmlImages at state " + node.name);
        };
        var imgKey = arrOfPair[0][0];
        var html = arrOfPair[0][1];

        if (typeof html === 'object') {
          if (data.fsm['defaultLang']) {
            if (html[data.fsm.defaultLang]) {
              html = html[data.fsm.defaultLang];
            } else {
              let firstLang = _.keys(html)[0];
              html = html[firstLang];
              dblogger.warn('WARNING: No images found in user\'s language: ' + data.fsm.defaultLang + ". showing: " + firstLang + ". -" + node.id + "-" + tick.process.summary());
            }
          }
        }
        // if its a file, load it
        if (html.charAt(0) !== '<') {
          htmlModel.get(html, folderName).then(function (htmlContents) {

            htmlModel.renderImage(tick, imgKey, htmlContents, node).then(function () {
              if (--imagesCount <= 0) {
                imgsResolve();
              }
            }).catch(function (ex) {
              dblogger.error('error in render images', ex);
              if (--imagesCount <= 0) {
                imgsResolve();
              }
            });
          }).catch((ex) => {
            dblogger.error('htmlModel.get failed:', ex);
          });
        } else {
          // if the html is inline refer to it by index and state
          htmlModel.renderImage(tick, imgKey, html, node).then(function () {
            imgsResolve();
          }).catch(function (ex) {
            dblogger.error('when rendering', ex);
            if (--imagesCount <= 0) {
              imgsResolve();
            }
          });
        }

      });
    }
  });
  return imgPromise;
}



/**
 * evaluate the template
 * @param process
 * @param promptTemplate
 * @return string
 */
messageBuilder.build = function (tick, fieldName, node) {
  var process = tick.process
  var folderName = fsmModel.getFSMSync(process.fsm_id).folderName;

  var data = node.alldata(tick);
  var ret = {};

  var promise = new Promise(function (resolve, reject) {
    try {

      // if there's a message
      if (node.prompt(fieldName) && node.prompt(fieldName).length !== 0) {
        // if the prompt() is an object, fine, stringify it
        var prompt = node.prompt(fieldName);
        // if object of multi-lingual prompt, choose the correct lang
        if (typeof prompt === 'object') {
          if (tick.process.properties()["defaultLang"]) {
            if (prompt[tick.process.properties()["defaultLang"]]) {
              prompt = prompt[tick.process.properties()["defaultLang"]];
            } else {
              let firstLang = _.keys(prompt)[0];
              prompt = prompt[firstLang];
              dblogger.warn('WARNING: No prompt found in user\'s language: ' + data.fsm.defaultLang + ". showing: " + firstLang + ". -" + node.id + "-" + tick.process.summary());
            }
          }
        }

        // if array of prompts, choose a random prompt
        if (prompt && _.isArray(prompt)) {
          prompt = prompt[_.random(0, prompt.length - 1)];
        }
        if (typeof prompt === 'object' && prompt !== null) {
          prompt = JSON.stringify(prompt);
        }
        try {
          ret.text = _.template(prompt)(data);
        } catch (err) {
          dblogger.error('ERROR IN prompt template  ' + node.id + "-" + tick.process.summary(), err);
          reject('ERROR IN prompt template  ' + node.id + "-" + tick.process.summary() + err);
        }
        if (ret.text) {
          ret.text = ret.text.trim();
        } else {
          ret.text = "";
        }
        // and then re-parse
        if (typeof prompt === 'object' && prompt !== null) {
          ret.text = JSON.parse(ret.text)
        }
      }

      // once images are there in the process, compile the view
      processImages(tick, node).then(function () {
        var payload = node.volatile(tick, "payload");
        node.volatile(tick, "payload", null);
        var viewObj = node.view();
        if (payload && (typeof payload !== 'object')) {
          var payloadData = payload;
          try {
            payloadData = _.template(payload)(data);
          } catch (err) {
            dblogger.error('ERROR IN payload template  ' + node.id + "-" + tick.process.summary(), err);
            reject('ERROR IN payload template  ' + node.id + "-" + tick.process.summary() + err);
          }
          ret.payload = payloadData;
          resolve(ret);
        } else if (viewObj) {
          var viewFilename = "";
          if (_.isObject(viewObj)) {
            if (data.fsm.defaultLang) {
              if (viewObj[data.fsm.defaultLang]) {
                viewFilename = viewObj[data.fsm.defaultLang];
              } else {
                let firstLang = _.keys(viewObj)[0];
                viewFilename = viewObj[firstLang];
                dblogger.warn('WARNING: No view found in user\'s language: ' + data.fsm.defaultLang + ". showing: " + firstLang + ". -" + node.id + "-" + tick.process.summary());
              }

            }
          } else if (_.isString(viewObj)) {
            viewFilename = viewObj;
          } else {
            dblogger.error('ERROR: View field is not well formmatted  ' + node.id + "-" + tick.process.summary());
            return reject('ERROR: View field is not well formmatted  ' + node.id + "-" + tick.process.summary());
          }
          if (typeof viewFilename === 'string') {
            viewModel.get(viewFilename, folderName).then(function (viewData) {
              try {
                var updatedData = node.alldata(tick);
                viewData = _.template(viewData)(updatedData);
                //viewData = viewData.replace(/"/g,"'");
              } catch (err) {
                dblogger.error('ERROR IN view template  ' + node.id + "-" + tick.process.summary(), err);
                reject('ERROR IN view template  ' + node.id + "-" + tick.process.summary() + err);
              }

              ret.payload = viewData;
              resolve(ret);

            }).catch((ex) => {
              dblogger.error('viewModel.get failed:', ex);
            });
          } else {
            try {
              var viewData = JSON.stringify(viewFilename);
              var updatedData = node.alldata(tick);
              viewData = _.template(viewData)(updatedData);
              //viewData = viewData.replace(/"/g,"'");
            } catch (err) {
              dblogger.error('ERROR IN view template  ' + node.id + "-" + tick.process.summary(), err);
              reject('ERROR IN view template  ' + node.id + "-" + tick.process.summary() + err);
            }

            ret.payload = viewData;
            resolve(ret);
          }

        }
        // else if (node.payload()) {
        //     var payloadData = node.payload();
        //     try {
        //         payloadData = _.template(node.payload())(data);
        //     } catch (err) {
        //         dblogger.error('ERROR IN payload template  ' + node.id + "-" + tick.process.summary(), err);
        //         return reject('ERROR IN payload template  ' + node.id + "-" + tick.process.summary() + err);
        //     }
        //     ret.payload = payloadData;
        //     resolve(ret);
        // } 
        else {
          resolve(ret);
        }

      }).catch(function (e) {
        dblogger.error('ERROR IN processImages  ' + node.id + "-" + tick.process.summary(), e);
        reject('ERROR IN processImages  ' + node.id + ":" + e + "-" + tick.process.summary());
      });;

    } catch (e) {
      dblogger.error('ERROR IN PROMPT for node ' + node.id + "-" + tick.process.summary(), e);
      reject('ERROR IN PROMPT:' + node.id + "-" + tick.process.summary() + e);
    }




  });

  return promise;
}
