var uuid = require("uuid");
var _ = require('underscore');
var utils = require("utils/utils");
var dblogger = require("utils/dblogger.js");
var Promise = require('promise');
var PipeManager = require('pipes/pipemanager');
var FSMManager;
var ProcessModel;
var Blackboard = require('./blackboard');
var Map = require('es6-map');
var Target = require('FSM/core/target');
var Tick = require('FSM/core/tick');
/**
 * Process holds the nodes and trees memory
 * @memberof module:Core
 */
class Process extends Blackboard {

  /**
   *Start a process and its children in memory
   */
  constructor(fsm, base) {
    super();
    _.extend(this, base);

    this.treeId = this.fsm_id = fsm.id;
    this.type = base.type || "processInstance";
    this.status = base.status || "active";
    this.channelId = base.channel && base.channel.toLowerCase();
    this.properties = this.options = fsm.properties;

    ProcessModel = ProcessModel || require('../../models/processmodel');

    //this.set('openNodes', [], fsm.id); // TODO: reload these as well

    // language -obsolete. now properties as a whole are supported
    if (fsm.properties) {
      fsm.properties.defaultLang = fsm.properties.defaultLang || "en";
      // backwards compatibility
      this.data('lang', fsm.properties.defaultLang);
      this.volatileAllData = fsm.properties.resetMemory;
      this.properties(fsm.properties)
    }


  }

  /**
   * bring in the properties as a whole
   * @param {*} props 
   */
  properties(props) {
    if (!props) {
      return this.data('properties');
    } else {
      return this.data('properties', props);
    }

  }
  /***
   * @typedef BreakpointData
   * @property nodeId 
   */
  /**
   * setBreakpoint 
   * @param {BreakpointData} breakpointReached 
   */
  setBreakpoint(bpData) {
    var breakpoints = this.volatile('breakpoints') || {};

    breakpoints[bpData.nodeId] = bpData;

    this.volatile('breakpoints', breakpoints);
  }

  /**
   * clear breakpoint
   * @param {string} nodeId 
   */
  clearBreakpoint(nodeId) {
    var breakpoints = this.volatile('breakpoints') || {};
    breakpoints[nodeId] = undefined;
    this.volatile('breakpoints', breakpoints);
  }

  isBreakpoint(nodeId) {
    let bps = this.volatile('breakpoints') || {};
    return !!bps[nodeId];
  }


  /**
   * returns the breakpoints set on this node
   * @param {string} nodeId 
   */
  breakpoint(nodeId) {
    let bps = this.volatile('breakpoints') || {};
    return bps[nodeId];
  }

  /**
   * save this process
   * @return a promise
   */
  save() {
    // save in the db the process
    return new Promise((resolve, reject) => {

      // save
      ProcessModel.upsert(this).then((resultDocID) => {

        resolve(resultDocID);
      }).catch((err) => {
        dblogger = require('utils/dblogger');
        dblogger.error('ProcessModel.upsert', this.summary(), err)
        reject(err);
      })
    });
  }

  // marker for logging
  summary() {
    return "tree:" + this.treeId + "/id:" + this.id;
  }
  /**
   * get the prompt for a state
   * @param stateMemberObj
   * @param key
   * @return {*}
   */
  getPrompt(stateMemberObj, key) {
    return this.getCurState()[stateMemberObj][key];

  }

  /**
   * set / empty nodes volatile
   * @param {Object} volMem 
   */
  resetVolatile(volMem) {
    if (volMem) {
      return this._volatileMemory = volMem;
    } else {
      var retMem = this._volatileMemory;
      this._volatileMemory = {};
      return retMem;
    }
  }

  /**
   * perform various maintenance tasks
   */
  tick() {

  }

  /**
   * setter/getter
   */
  volatile(key, value) {
    if (arguments.length === 0) {
      return this._volatileMemory["process-volatile-memory"];
    } else if (arguments.length >= 2) {
      return this._volatileMemory["process-volatile-memory"][key] = value;
    } else {
      return this._volatileMemory["process-volatile-memory"][key];
    }
  }




  /**
   * sets process data. 
   * pass no arguments to get all data
   * pass explicit null key to replace all data with value
   * @param {string} [key] 
   * @param {*} [value] 
   */
  data(key, value) {

    var processData = this.get('data') || {};
    if (arguments.length === 0) {
      return processData;
    } else if (arguments.length === 1) {
      return processData[key];
    } else {
      // convert to number if needed
      if (!utils.safeIsNaN(value)) {
        value = parseFloat(value);
      }

      if (key !== null && key != "") {
        try {
          eval("processData." + key + "= value");
        } catch (ex) {
          dblogger.error('error in global/process.data(' + key + "," + value + "):", ex);
          processData[key] = value;
        }
      } else {
        dblogger = require('utils/dblogger');
        dblogger.assert(_.isObject(value), "value needs to be an object to replace all of processData");
        processData = value;
      }
      return this.set('data', processData);
    }
  }

  /**
   * Get context entities ids and load context entity 
   * instances into volatile memory
   */
  loadContextEntities() {

    FSMManager = require('FSM/fsm-manager');
    let curCtxIds = this.data('currentContextEntityIds');
    if (!curCtxIds) {
      return;
    }

    let tree1 = FSMManager.getBehaviorTree({
      id: curCtxIds.treeId
    });
    var node = tree1 && tree1.getNode(curCtxIds.nodeId);
    if (!node || !node.contextManager) {
      dblogger = require('utils/dblogger');
      dblogger.error('node should have a context manager. current context entity ids:', curCtxIds);
      return;

    }
    var tick = new Tick(this, tree1, null, curCtxIds.tickDepth);
    var currentContextEntities = {
      contextManager: node.contextManager,
      tick: tick
    };
    this.currentContextEntities(currentContextEntities);
  }

  /**
   * sets the current contextManager and tick
   * @param {*} crntCtxEtts 
   */
  currentContextEntities(crntCtxEtts) {
    if (arguments.length === 1) {
      this.data('currentContextEntityIds', crntCtxEtts && {
        nodeId: crntCtxEtts.contextManager.node.id,
        treeId: crntCtxEtts.tick.tree.id,
        tickDepth: crntCtxEtts.tick.depth
      });
      return this.volatile('currentContextEntities', crntCtxEtts);
    } else {
      return this.volatile('currentContextEntities');
    }
  }

  getNonVolatile() {
    var retProcess = {};
    if (this.volatileAllData) {
      return null;
    }
    for (var k in this) {
      if (k !== "_volatileMemory" && k !== "messages") {
        retProcess[k] = this[k];
      }
    }

    // some special data related ones
    retProcess.data = () => {
      return this.data();
    }

    return retProcess;
  }

}

module.exports = Process;
