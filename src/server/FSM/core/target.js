/**
 * Target - a queue of targets 
 *
 * Copyright (c) 2017 Servo Labs Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * 
 **/
var config = require('config');
/**
 * a target object maintaining a queue of outputs from classfiers layers 
 *
 *@memberof module:Core
 **/

class Target {

  /**
   * constructor
   */
  constructor() {
    /** @member {number} lastTargetId - running index of the target id*/
    this.lastTargetId = 0;
    /** @member {Array<TargetObject>} targetObjs - target queue*/
    this.targetObjs = [];
    this.passedThruPipe = false;
  }

  /***
   * @typedef {TargetObject}
   * @property {Object} messageObj - an object with the message details from NLU
   * @property {Any}
   */
  /**
   * add a target object to the queue
   * @param {TargetObject} targetObj 
   */
  add(targetObj) {
    var dblogger = require('utils/dblogger');
    dblogger.flow('Add target', targetObj.messageObj);
    this.passedThruPipe = false;
    // start searching from the top 
    targetObj.searchContextDownwards = true;

    targetObj.usage = targetObj.usage || {};

    var id = this.lastTargetId + 1;
    if (this.lastTargetId === 9007199254740992) { // maxint: https://stackoverflow.com/questions/21350175/max-integer-value-in-javascript
      this.lastTargetId = 0;
    }
    this.lastTargetId = id;
    targetObj.id = id;

    this.targetObjs.unshift(targetObj);


  }

  /**
   * remove target from the queue and return it
   */
  remove() {
    return this.targetObjs.pop();
  }

  /**
   * remove all targets from the queue
   */
  removeAll() {
    this.targetObjs = [];
  }

  /**
   * remove all targets until an NLU intent is found
   */
  removeUntilIntentFound() {
    //this.remove();
    while (this.get() && !this.getIntent()) {
      this.remove();
    }
  }

  /**
   * @return [TargetObject] - current target object in queue
   */
  get() {
    return this.targetObjs[this.targetObjs.length - 1];
  }

  /**
   * @return {string} last id 
   */
  id() {
    return this.get().id;
  }

  /**
   * a wakeup message is relevant to non-push clients like Alexa
   * @return {boolean} true if the current message received is a wakeup event
   * 
   */
  isWakeUp() {
    return this.getMessageObj() && this.getMessageObj().wakeUp;
  }

  getIntent() { // for backward compat.
    return this.getMessageObj().intentId;
  }

  /**
   * @return 
   */
  getMessageObj() {
    return this.get() && this.get().messageObj;
  }

  /**
   * @return {boolean} true if a target exist
   */
  exists() {
    return this.getMessageObj();
  }


  /**
   * get entity by name. if an array of entities with same name, get then entity by index 
   * @param {string} entityName 
   * @param {number} [index=0] 
   */
  getEntity(entityName, index = 0) {
    var entity = this.getMessageObj() &&
      this.getMessageObj().entities &&
      this.getMessageObj().entities[entityName];
    return (Array.isArray(entity) ? entity[index] : entity);
  }

  /**
   * set a flag for using this entity for context mapping
   * @param {string} ettName 
   * @param {number} ettIndex - the index of this entity in the message from NLU (in case there were several entities or a composite entity)
   * @param {number} depth - the depth at which it was used for mapping
   * @return {number} 
   */
  useEntity(ettName, ettIndex, depth) {
    if (!this.get().usage[ettName + ":" + ettIndex]) {
      return this.get().usage[ettName + ":" + ettIndex] = depth;
    } else {
      return this.get().usage[ettName + ":" + ettIndex];
    }

  }
  /**
   * 
   * @param {string} ettName 
   * @param {number} ettIndex - the index of this entity in the message from NLU (in case there were several entities or a composite entity)
   * @return {boolean} true if this entity was used
   *  @param {number} depth - the depth at which it was used for mapping
   */
  wasEntityUsedMoreSpecifically(ettName, ettIndex, depth) {
    return this.get().usage[ettName + ":" + ettIndex] >= depth;
  }

  /**
   * @return {boolean} true if this target was used for context search
   */
  usedForSearch() {
    return this.get().targetUsedForSearch;
  }


  /**
   * set a flag that this target was used for search
   */
  useForSearch(x = true) {
    if (this.get()) {
      this.get().targetUsedForSearch = x;
    }
  }

  getTargets() {
    return this.targetObjs;
  }

}

module.exports = Target;
