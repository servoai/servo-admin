/**
 * ContextManager
 *
 * Copyright (c) 2017 Servo Labs Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 **/
var statsManager = require('FSM/statsManager');
var _ = require('underscore');

var dblogger = require('utils/dblogger');
var b3 = require('FSM/core/b3');
var Tick = require('FSM/core/tick');
var utils = require('utils/utils');

const CONTEXTMEMKEY = 'contextMem';

/**
 * Manages a node with child contexts. 
 * 
 * @memberof module:Decorators
 */
class ContextManager {

  static CONTEXTMEM() {
    return CONTEXTMEMKEY;
  }
  static EXPIREMINUTES() {
    return 600;
  }

  /**
   * ContextManager initializer
   * @param {BaseNode} node 
   */
  constructor(node) {
    this.node = node;

  }

  /**
   * find my ContextManager's parent's ContextManager
   * @param {Tick} tick
   */
  findNextContextManagerEntities(tick) {
    var myContextManager = this.node.findContextManagerEntities(tick);

    var parentEtts = myContextManager.node.parentEntities(tick);

    if (!parentEtts.node) {
      return {
        node: undefined,
        tick: undefined
      }
    } else return parentEtts.node.findContextManagerEntities(parentEtts.tick);
  }

  /**
   * @typedef FoundContext
   * @property {Tick} tick
   * @property {boolean}[isRootContext]
   * @property {number} index - an index of new context child of this.node.
   * @property {number} prevIndex - previous index of new context child of this.node.
   * @property {boolean} [helper] true if this is a helper context 
   * @property {number} [score]
   * @property {Object} [contextManager]
   */

  /**
   * @typedef {Object} KeyValuePair
   * @property {string} key
   * @property {string} value 
   */
  /**
   * @typedef {Object} MessageObject
   * @property {string} intentId
   * @property {Array<KeyValuePair>} entities
   * @property {number} score
   * 
   */
  /****
   *
   * "search": search up to find the right context in the right level to switch to
   * "map": map the message entities to the expected entities 
   * "switch": when u have a prev context, switch to a new one
   * "select": no context selected yet, need to select
   * Search & mapping up
   * 
   *  for context switch & select:
   * 1. search by intent. if found
   *  a. switch
   *  b. map entities
   * 
   * 2. for context select, otherwise search by entities. if found(meaning, we have a number >= 1 of entities match)
   *   a. map the entities iteratively by going up from the level we found 
   */


  /**
   * for this manager, find the right context
   * try first by intents, otherwise try entities, otherwise helper
   * @param {Tick} tick
   * @param {string} intentDirection - search direction for intents: "downwards" or "upwards", or both when undefined 
   * @return {FoundContext}
   */
  findContextToSelect(tick, intentDirection = undefined) {
    var newContext = {
      index: -1,
      prevIndex: -1,
      score: -1,
      helper: false,
      tick: tick
    };
    var contextsParams = this.node.contextProperties();
    // if (tick.target.exists()) {
    var msgObj = tick.target.getMessageObj();
    // see intent
    var intentId = msgObj && msgObj.intentId;
    intentId = intentId || "None";
    // if an intent exists
    _.each(contextsParams, (ctxParam, index) => {

      var ctxIntentId = ctxParam.intentId;
      if (!ctxIntentId) {
        ctxIntentId = ctxParam[intentDirection] && ctxParam[intentDirection].intentId;
      }
      // if this one is the right context
      if (ctxIntentId) {
        var intents = Array.isArray(ctxIntentId) ?
          ctxIntentId : [ctxIntentId];
        var found = intents.find((elem) => {
          return intentId.toLowerCase() === elem.toLowerCase() || !!(new RegExp(elem, "i")).exec(intentId.toLowerCase());
        });
        if (found) {
          newContext = {
            index: index,
            prevIndex: this.node.currentChildIndex(tick),
            score: utils.safeIsNaN(msgObj && msgObj.score) ? 1.0 : msgObj.score,
            helper: false,
            tick: tick
          };
        }
      }
    });


    /**try to find a context using the entities:  no intent,or couldnt find a context for the intent.
     *  see if there is a **current** context, see if it has such entity 
     * otherwise, check sibling contexts*/
    // we chose not to work **always** with entities of other contexts
    if (newContext.score < 0) {
      let ettMatchObj = this.selectContextWithMaxEntities(tick);
      if (ettMatchObj.index > -1 && ettMatchObj.count > 0) {
        newContext = {
          index: ettMatchObj.index,
          prevIndex: undefined,
          score: ettMatchObj.count * this.node.entityWeight,
          helper: false,
          tick: tick
        };
      }
    }


    // if nothign was found here, find helper context as default
    var helperContextIndex, helperContext;
    if (newContext.score < 0) {
      helperContext = contextsParams.find((ctxParam, index) => {
        if (ctxParam.helper) {
          helperContextIndex = index;
          return true;
        }
      });
      // if a helper context exists here,  return it as a default
      if (helperContext) {
        newContext = {
          index: helperContextIndex,
          prevIndex: this.node.currentChildIndex(tick),
          helper: true,
          score: -1,
          tick: tick
        };
      }
    }

    newContext.contextManager = this;
    return newContext;
  }

  /**
   * selects an index in the contexts that has the maximal entity match
   * @param {*} tick 
   * @param {*} msgObj 
   */
  selectContextWithMaxEntities(tick) {
    let maxEttCount = 0;
    let retIndex = -1;
    let ettCount = -1;
    var ctxParams = this.node.contextProperties();
    for (let i = 0; i < ctxParams.length; i++) {

      ettCount = this.mapEntitiesToContext(tick, ctxParams, i, true);

      if (ettCount > maxEttCount) {
        maxEttCount = ettCount;
        retIndex = i;
      }
    }

    return {
      index: retIndex,
      count: maxEttCount
    };
  }
  /**
   * 
   * @param {string} intent id
   * @return {Boolean} true if this intent represent a none - meaning, no intent was found by the NLU 
   */
  isNone(value) {
    return value && value.toLowerCase() === 'none';
  }
  /**
   * set the current context entities
   * if this is the root, its not considered a context to 
   * @param {Tick} tick
   * @param {ContextManagerEntities} [contextEntities]
   */
  setCurrentContext(tick, contextEntities) {
    let nodeId = (contextEntities && contextEntities.node.id) || this.node.id;
    var fsmManager = require('FSM/fsm-manager');
    var btRoot = fsmManager.getBehaviorTree({
      id: tick.process.fsm_id
    });
    // never save the root as a context
    if (nodeId !== btRoot.root.id && tick.tree.id !== tick.process.properties().pipeTree) {
      tick.process.currentContextEntities({
        contextManager: (contextEntities && contextEntities.node.contextManager) || this,
        tick: (contextEntities && contextEntities.tick) || tick
      });
    }
  }

  /**
   * @return {boolean} true if this child is the context
   */
  childInContext(tick, childIndex, msgObj) {

    let intentId = msgObj && msgObj.intentId;
    let ctxParam = this.node.contextProperties()[childIndex];
    // if an intent exists
    if (intentId) {

      // if this one is the right context
      if (ctxParam.intentId) {
        var intents = Array.isArray(ctxParam.intentId) ?
          ctxParam.intentId : [ctxParam.intentId];
        var found = intents.find((elem) => {
          return intentId.toLowerCase() === elem.toLowerCase() || new RegExp(elem).exec(intentId.toLowerCase());
        })
        return found;
      }
      return (ctxParam.helper);
    }
    // // if not, try entities
    // _.each(msgObj.entities, (val, key) => {
    //     _.each(ctxParam && ctxParam.entities, (ett, index) => {
    //         if (ett.entityName === key) {
    //             return true;
    //         }
    //     })

    // });
  }


  /**
   * build the tree of contextManagers
   * @param {ContextManager} parentContextManager 
   * @param {BaseNode} node - current node
   */
  buildManagerTree(parentContextManager, node) {

    if (node.contextManager) {
      node.contextManager.parent = parentContextManager;
      if (parentContextManager) {
        parentContextManager.child = node.contextManager;
      }
      // now this becomes the parent
      parentContextManager = node.contextManager;
    }

    if (node.children) {
      node.children.forEach((child) => {
        this.buildManagerTree(parentContextManager, child);
      });
    } else if (node.child) {
      this.buildManagerTree(parentContextManager, node.child);
    }
  }

  /**
   * map all the entities from this context and up
   * @param {FoundContext} selectedContext 
   */
  mapAllEntitiesUp(selectedContext) {

    var parentetts = {
      tick: selectedContext.tick,
      node: selectedContext.contextManager.node
    };
    var contextManager = selectedContext.contextManager;

    let loopStop = false;
    // go up the contextmanager's tree
    while (!loopStop) {

      contextManager.mapEntitiesToContext(parentetts.tick, parentetts.node.contextProperties(), parentetts.node.currentChildIndex(parentetts.tick));
      // try next ones
      parentetts = contextManager.findNextContextManagerEntities(parentetts.tick, parentetts.node);


      // if reached the root, or no contexts to map
      if (!parentetts.node || !parentetts.node.contextProperties()[parentetts.node.currentChildIndex(parentetts.tick)]) {
        // signal loop stop
        loopStop = true;
      } else {
        contextManager = parentetts.node.contextManager;
      }

    }
  }

  /**
   * switch between contextFrames; add a context if needed
   * @param {Tick} tick
   * @param {number} selectedConvoIndex 
   * @param {number} [prevSelectedConvoIndex]
   * @return {boolean} true if switched 
   */
  switchContext(tick, selectedConvoIndex, prevSelectedConvoIndex = -1) {

    // set the context here
    // this.setCurrentContext(tick);
    // set the context at the node
    this.node.setContextChild(tick, selectedConvoIndex);

    // map 
    this.mapAllEntitiesUp({
      tick: tick,
      index: selectedConvoIndex,
      prevIndex: prevSelectedConvoIndex,
      contextManager: this
    });

    if (selectedConvoIndex === prevSelectedConvoIndex) {
      return false;
    }

    var contextFrames = this.getContextFrames(tick);

    var framesWithoutSelectedContext = contextFrames.filter((item) => {
      return (item.index !== selectedConvoIndex);
    });

    // push new frame always last
    framesWithoutSelectedContext.push({
      index: selectedConvoIndex, // new index
      timestamp: Date.now(),
      treeId: tick.tree.id
    });

    // now remove any other non-context frame. WE ONLY ALLOW ONE    
    contextFrames = framesWithoutSelectedContext.filter((item) => {
      if (this.node.nonContextChild(selectedConvoIndex)) {
        return (item.index === selectedConvoIndex) || !this.node.nonContextChild(item.index);
      } else {
        return true;
      }

    });

    // now, find previous and save last opened leaf
    if (utils.isTruthyOr0(prevSelectedConvoIndex) && prevSelectedConvoIndex >= 0) {
      var oldLeafNode = this.node.findCurrentWaitNode(tick, this.node.contextChildren()[prevSelectedConvoIndex]);
      var oldContext = contextFrames[framesWithoutSelectedContext.length - 2];
      dblogger.flow('switchContext from  ' +
        prevSelectedConvoIndex + 'to ' + selectedConvoIndex + ' at ' + this.node.summary(tick), 'old leaf:', oldLeafNode.node && oldLeafNode.node.id);
      if (!oldLeafNode.node || !oldContext) {
        this.node.log(tick, 'no leaf node, could happen when starting a tree');
      } else {

        oldContext.leafNodeId = oldLeafNode.node && oldLeafNode.node.id;
        oldContext.treeId = oldLeafNode.tick.tree.id;
      }


    }

    // re-save all frames
    this.setContextFrames(tick, contextFrames);

    // save some stats
    statsManager.addConversationStart(tick, selectedConvoIndex, this.node);


    return true;
  }

  /**
   * get the context array
   * @param {Tick} tick
   */
  getContextFrames(tick) {
    return tick.process.get('contextFrames', tick.tree.id, this.node.id) || [];
  }

  /**
   * set the context array
   * @param {Tick} tick
   */
  setContextFrames(tick, cframes) {
    cframes.sort((a, b) => {
      return a.timestamp > b.timestamp;
    });
    return tick.process.set('contextFrames', cframes, tick.tree.id, this.node.id);
  }

  /**
   * map entities to parent context
   * @param {Tick} tick
   */
  close(tick, status) {

    var child = this.node.currentChildIndex(tick);
    if (child < 0 || child === undefined) {
      // if not running - ended, always clear, so no entities will be left for next round
      this.clearAllContexts(tick);

      return;
    }
    this.mapEntitiesToParent(tick);
    _.each(this.node.contextProperties()[child].entities, (fieldMap) => {

      // if exists on target, then it was mapped, set a flag about it so next contexts wont use it
      if (!_.isUndefined(tick.target.getEntity(fieldMap.entityName, fieldMap.entityIndex))) {
        var depth = tick.target.useEntity(fieldMap.entityName, fieldMap.entityIndex, tick.depth);
        if (depth != tick.depth) {
          dblogger.log('tried to useEntity less specifically (it was used already):', tick.target, fieldMap.entityName, tick.tree.id);
        }
      }

    });

    // if success/running
    if (status != b3.FAILURE() && status != b3.ERROR()) {
      statsManager.setLastRun(tick, child, this.node);
    } // set a flag on mapped entities so next context would not deal with them


    // if succesful, save in memory, and clear this context
    if (status === b3.SUCCESS()) {
      this.saveLastContext(tick);
    }

    // if not running - ended, always clear, so no entities will be left for next round
    this.clearAllContexts(tick);

    // this.returnContextToParent(tick);

    // since we might close after a context switch (w/out proper closing), we must close explicitly the children
    for (let child of this.node.contextChildren()) {
      child._close(tick);
    }

    // reset for next open
    this.node.local(tick, 'runningChild', -1);
  }

  /**
   * a context-managed node forwarded execution from this function
   * if the node.tick was succesful, we switch to previous context. if such a \
   * switch didnt happen (ie there was no previous context) we return context to parent 
   * ContextManager 
   * @param {Tick} tick 
   * @return {TickStatus} status
   */
  tick(tick) {

    var selected = this.node.contextChildren()[this.node.currentChildIndex(tick)];

    var status = selected._execute(tick);

    // A SWITCHED-TO ROUTE SHOULD NOT FAIL. IF IT DOES, ITS PARENT FAILS
    if (status === b3.SUCCESS()) {
      // if more contextFrames, this selector is still not done
      if (this.switchToPrevContext(tick)) {
        this.node._enter(tick); // simulate entry so THIS node wont be closed!
        status = b3.RUNNING();
      } else {
        // TODO: MOVE TO close()
        this.returnContextToParent(tick);
      }
    }

    return status;

  }
  /**
   * map context fields to parent
   * @param {Tick} tick
   * @param {Object} [contextProperties] - properties of the context-managed node 
   */
  mapEntitiesToParent(tick, contextProperties) {
    var parentContextManagerEtts = this.findNextContextManagerEntities(tick);
    if (!parentContextManagerEtts.node) {
      return;
    }

    dblogger.assert(!utils.safeIsNaN(this.node.currentChildIndex(tick)), 'child has not been selected correctly at ' + this.node.summary(tick) + ' - check intents/contexts properties against message');
    _.each(contextProperties || this.node.contextProperties()[this.node.currentChildIndex(tick)].entities, (fieldMap) => {
      var value = this.getContextField(tick, fieldMap.contextFieldName);
      if (!_.isUndefined(value)) {
        parentContextManagerEtts.node.contextManager.setContextField(parentContextManagerEtts.tick, fieldMap.contextFieldName, value);
      } else if (!fieldMap.leaveIfNoEntity) {
        parentContextManagerEtts.node.contextManager.setContextField(parentContextManagerEtts.tick, fieldMap.contextFieldName, undefined);
      }
    });
  }

  /**
   * get the key/value from first context-managed ancestor which keeps it
   * @param {Tick} tick
   * @param {string} key 
   * @return {string}  value
   */
  getContextField(tick, key) {
    var node = this.node;
    var value;
    var contextManager = this;

    do {
      dblogger.assert(contextManager, "node must have a contextManager");
      value = contextManager.getContextMemory(tick)[key];
      if (_.isUndefined(value)) {
        var nextCntxtMgrEtts = contextManager.findNextContextManagerEntities(tick);
        node = nextCntxtMgrEtts && nextCntxtMgrEtts.node;
        tick = nextCntxtMgrEtts && nextCntxtMgrEtts.tick;
        contextManager = node && node.contextManager;
      }
    } while (_.isUndefined(value) && node);

    return value;
  }

  /**
   * saves the ctxMgrEtts as the current context entities (currentContextEntities)
   * @param {Tick} tick 
   * @param {ContextManagerEntities} ctxMgrEtts 
   */
  saveGlobalContext(tick, ctxMgrEtts) {
    tick.process.currentContextEntities(ctxMgrEtts.node && {
      contextManager: ctxMgrEtts.node && ctxMgrEtts.node.contextManager,
      tick: ctxMgrEtts.tick
    });
  }

  /**
   * when done with this context, called to return context to parent 
   * @param {Tick} tick
   * @return {boolean} true if parent node
   */
  returnContextToParent(tick) {
    var ctxMgrEtts = this.findNextContextManagerEntities(tick);

    if (ctxMgrEtts.node) {
      this.setCurrentContext(tick, ctxMgrEtts);
    }



    return !!ctxMgrEtts.node;
  }


  /**
   * maps from the context expected entities to the context memory fields, 
   * according to the map defined at the context-managed node properties
   * @param {Tick} tick 
   * @param {Array<EntitiesToContextMapItem>} map - a map of expected context entities to context fields 
   * @param {number} childIndex - current child Index
   * @param {boolean?} countOnly - count possible mappings only, no actual maps  
   * @returns {number} - number of maps
   */
  mapEntitiesToContext(tick, map, childIndex, countOnly = false) {
    dblogger.assert(!utils.safeIsNaN(childIndex), "No current context");
    if (!tick.target.exists()) {
      return;
    }
    let numberOfMaps = 0;
    let contextDetails = map[childIndex];
    dblogger.assert(contextDetails, "no context details - bug");
    //dblogger.assert(contextDetails, "No context details. maybe a context is missing on composite " + this.node.summary(tick));
    _.each(contextDetails.entities, (ett) => {
      var entityValue = tick.target.getEntity(ett.entityName, ett.entityIndex);
      if (!_.isUndefined(entityValue) && !tick.target.wasEntityUsedMoreSpecifically(ett.entityName, ett.entityIndex, tick.depth)) {
        if (!countOnly) {
          this.setContextField(tick, ett.contextFieldName, entityValue);
          tick.target.useEntity(ett.entityName, ett.entityIndex, tick.depth);

        }
        numberOfMaps++;

        dblogger.flow('mapEntitiesToContext', 'target value ' + entityValue + ' mapped to ' + ett.entityName + ' at ' + this.node.summary(tick))
      } else if (!ett.leaveIfNoEntity && !tick.target.wasEntityUsedMoreSpecifically(ett.entityName, ett.entityIndex)) {
        if (!countOnly) {
          this.setContextField(tick, ett.contextFieldName, undefined);
          tick.target.useEntity(ett.entityName, ett.entityIndex, tick.depth);
        }
        numberOfMaps++;
        dblogger.flow('mapEntitiesToContext', 'target value ' + entityValue + ' mapped to ' + ett.entityName + ' at ' + this.node.summary(tick))
      }
    });
    //dblogger.flow('mapEntitiesToContext mapped ' + numberOfMaps + ' countOnly = ' + countOnly);
    return numberOfMaps;
  }

  /**
   * the context memories are set at the relevant child's local memory, under CONTEXTMEM() key
   * 
   */
  /**
   * set a context field
   * ASSUMES the call to this function can come only from within the current context
   * @param {Tick} tick
   * @param {string} fieldName 
   * @param {string} value 
   * @return {string} value set 
   */
  setContextField(tick, fieldName, value) {

    var contextMem = this.getContextMemory(tick);
    try {
      eval("contextMem." + fieldName + "= value");
    } catch (ex) {
      dblogger.error('error in setContextField(' + fieldName + "," + value + "):", ex);
      contextMem.fieldName = value;
    }


    // currentContextChild always point to the right context
    this.node.currentContextChild(tick).local(tick, ContextManager.CONTEXTMEM(), contextMem);

    return value;
  }


  /**
   * get full object of current selected context memory
   * @param {Tick} tick 
   * @return {Object}
   */
  getContextMemory(tick) {
    let curChild = this.node.currentContextChild(tick);
    return curChild ? (curChild.local(tick, ContextManager.CONTEXTMEM()) || {}) : {};
  }

  /**
   * clears current selected context memory
   * @param {Tick} tick 
   */
  clearContext(tick) {
    this.node.currentContextChild(tick).local(tick, ContextManager.CONTEXTMEM(), {});
  }

  /**
   * clears all  context memories at the current context manager
   * @param {Tick} tick 
   */
  clearAllContexts(tick) {
    for (let child of this.node.contextChildren()) {
      child.local(tick, ContextManager.CONTEXTMEM(), {});
    }
    this.setContextFrames(tick, []);
  }

  /**
   * saves current context memeory at the lastContext field at the current context-managed node
   * not used 
   * @param {Tick} tick 
   * @private
   */
  saveLastContext(tick) {
    var curContext = this.getContextMemory(tick);
    var contextForSaving = {
      data: curContext,
      timestamp: Date.now()
    };

    // save last
    this.node.local(tick, 'lastContext', contextForSaving);
  }

  /**
   * pop last context, set a new selected convo, close and re-open
   * @param {Tick} tick
   * @return {boolean} true if there are more contextFrames
   */
  switchToPrevContext(tick) {

    var contexts = this.getContextFrames(tick);
    //dblogger.assert(contexts.length, 'must have at least one root context')
    // POP
    contexts.pop();
    // save
    this.setContextFrames(tick, contexts);

    // SWITCH TO PREVIOUS, if any 
    if (!contexts.length) {
      return false;
    }
    var prevContext = contexts[contexts.length - 1];
    dblogger.flow('switchToPrevContext ' + prevContext.index)
    var contextSwitched = this.switchContext(tick, prevContext.index);
    if (!contextSwitched) {
      return false;
    }
    // if needed, replay last leaf
    // ===========================
    // now re-open that one child (so any outstanding requests could be cancelled?)
    var fsmManager = require('FSM/fsm-manager');
    var bt = fsmManager.getBehaviorTree({
      id: prevContext.treeId
    });
    dblogger.assert(bt, 'Cant find bt when switching contextFrame', prevContext.treeId);

    // re-open the last leaf. so if there was a question, ask again
    // TODO: send a message of returning to context
    var leafNode = bt.nodes[prevContext.leafNodeId];
    if (leafNode && leafNode.properties && leafNode.properties.replayActionOnReturnFromContextSwitch) {
      var newTick = new Tick(tick.process, bt, tick.target, tick.depth);
      // TODO: create tick as if it was really reached from a regular tick 
      // the closeme only cares about the tree id TODO: make bt.closeCurrentLeaf()
      leafNode._closeMe(newTick);

      leafNode.open(newTick);
    }




    // start afresh - remove all targets 
    // TODO: a more accurate would be removeUntilIntent and remove
    //tick.target.removeAll();

    return true;
  }
}

module.exports = ContextManager;
