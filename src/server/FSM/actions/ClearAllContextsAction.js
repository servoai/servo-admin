// namespace:
var b3 = require('FSM/core/b3');
var Action = require('FSM/core/action')


/**
 * 
 * find current contextmanager and clear all its contexts.
 *  @memberof module:Actions 
 **/
class ClearAllContextsAction extends Action {

  constructor() {
    super();


    this.title = this.name = 'ClearAllContextsAction';
  }
  /**
   * Tick method.
   * @param {Tick} tick A tick instance.
   * @return {TickStatus} Always return `b3.SUCCESS`.
   * @private
   **/
  tick(tick) {

    this.clearAllContexts(tick);

    return b3.SUCCESS();
  }
}
module.exports = ClearAllContextsAction;
