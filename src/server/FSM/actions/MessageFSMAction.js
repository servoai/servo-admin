// namespace:
var b3 = require('FSM/core/b3');
var Action = require('FSM/core/action')
var _ = require('underscore');
var dblogger = require('utils/dblogger');


/**
 * Send a new message to the FSM, as if it came from the user. 
 *  @memberof module:Actions
 **/
class MessageFSMAction extends Action {

  constructor(settings) {
    super();
    this.title = this.name = 'MessageFSMAction';
    /**
     * Node parameters
     * @property parameters
     * @type {Object}
     * @property {ExpressionString} parameters.text
     * @property {ExpressionString} parameters.text
     */
    this.parameters = _.extend(this.parameters, {
      'text': '',
      'intentId': ''
    });
    settings = settings || {};
    if (_.isEmpty(settings.text) || _.isEmpty(settings.intentId)) {
      console.error("text and intentId are obligatory parameters");
    }

    this.description = "Send a new message to the FSM, as if it came from the user. " +
      "text and intentId are strings - meaning, if a template is needed use <%= %>";

  }

  open(tick) {
    this.waitCode(tick, undefined);
  }
  /**
   * Tick method.
   *
   * @private tick
   * @param {Tick} tick A tick instance.
   * @return {Constant}  return `b3.SUCCESS`.
   **/
  tick(tick) {

    if (!this.waitCode(tick)) {
      this.waitCode(tick, b3.RUNNING());

      // manipulate the target object to replace the text and intentId
      // we assume there'a  a target
      dblogger.assert(tick.target, "this action uses the current target. do not approach if no target. TODO: make a target");

      var alldata = this.alldata(tick);
      var text = _.template(this.properties.text)(alldata);
      var intentId = _.template(this.properties.intentId)(alldata);

      var curTarget = tick.target.get();

      var messageObj = curTarget.messageObj;
      dblogger.assert(messageObj, "this action uses the current message object. TODO: make a target");
      messageObj = messageObj || {};
      messageObj.intentId = intentId;
      messageObj.text = text;
      messageObj.queueAfterDoneRunning = true;
      tick.target.remove();

      var FSM = require('FSM/fsm-manager');
      FSM.actOnProcess(messageObj, tick.process).then(() => {
        // acted ( message added to q)
        this.waitCode(tick, b3.SUCCESS());
      }).catch((ex) => {
        dblogger.error("Error in sending message " + this.properties.textField + " to " + tick.process.summary(), ex);
        this.waitCode(tick, b3.ERROR());
      });

    }
    return this.waitCode(tick);
  }
}
module.exports = MessageFSMAction;
