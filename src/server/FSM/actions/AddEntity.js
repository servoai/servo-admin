var b3 = require('FSM/core/b3');
var _ = require('underscore');
var Action = require('FSM/core/action');
var utils = require('utils/utils');
var dblogger = require('utils/dblogger');

/**
 * Set fields across composite (global,context, volatile, local and message) memories. 
 * entityName and fieldValue should have a dot notation with the object name. Eg: message.chat_message, context.amount etc ';
 *  @memberof module:Actions
 */
class AddEntity extends Action {

  /**
   * 
   * @param {*} settings 
   */
  constructor(settings) {
    super();
    this.title = this.name = 'AddEntity';

    /**
     * Node parameters.
     *
     * @property parameters
     * @type {Object}
     * @prop {string} parameters.fieldValue
     *  @prop {string} parameters.entityName
     **/
    this.parameters = {
      'entityName': '',
      'fieldValue': ''

    };
    this.description = 'Add an entity from global,context, volatile and message memories.  fieldValue should have a dot notation with the object name. Eg: message.chat_message, context.amount etc ';
    settings = settings || {};
    if (utils.isEmpty(settings.entityName)) {
      console.error("entityName parameter in AddEntity is an obligatory parameter");
    } else if (utils.isEmpty(settings.fieldValue)) {
      console.error("fieldValue parameter in AddEntity is an obligatory parameter");
    }
  }

  /**
   * Tick method.
   *
   * @private tick
   * @param {Tick} tick A tick instance.
   * @return {TickStatus} A status constant.
   **/
  tick(tick) {
    try {
      var data = this.alldata(tick);

      var value = _.template(utils.wrapExpression(this.properties.fieldValue))(data);
      tick.target.getMessageObj().entities[this.properties.entityName] = tick.target.getMessageObj().entities[this.properties.entityName] || [];
      tick.target.getMessageObj().entities[this.properties.entityName].push(value);
      tick.target.useForSearch(false);


      return b3.SUCCESS();
    } catch (err) {
      dblogger.error("Error at AddEntity: " + this.summary(tick) + ":" + err.message);
      return b3.FAILURE();
    }
  }


  /**
   * @return {Array<Validator>}
   */
  validators(node) {

    function validCompositeField(field) {

      return field && !(field.indexOf('message.') === 0 || field.indexOf('fsm.') === 0 || field.indexOf('context.') === 0 || field.indexOf('global.') === 0 || field.indexOf('volatile.') === 0);
    }

    return [{
      condition: validCompositeField(node.properties.entityName),
      text: "entityName should NOT start with message., context., global. fsm. or volatile."
    }];
  }
}

module.exports = AddEntity;
