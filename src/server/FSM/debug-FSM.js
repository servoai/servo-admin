var fsmModel;
var processModel = require("models/processmodel.js");
var _breakpointReached;
var FSMManager;
var _ = require('underscore');
var dblogger = require('utils/dblogger');

/**
 * helper function
 * @param {*} fsm 
 * @param {*} bpData 
 */
function startOneProcess(fsm, bpData) {
  FSMManager = require('FSM/fsm-manager');
  var chatsim = require('chat/chatsim').getInst();
  var messageObj = chatsim.createMessageObject('reciepient-' + bpData.processId, bpData.processId, "", bpData.fsmId, {});

  return new Promise((resolve) => {
    FSMManager.startOneProcess(fsm, messageObj, bpData.processId, {
      volatileAllData: true
    }).then((process1) => {

      // the tick would start by the chatsim message
      // FSMManager = require('FSM/fsm-manager');
      // FSMManager.tickStart(fsm.id, process1);

      process1.volatile('atABreakpoint', false);
      dblogger.flow("process " + process1.id + " created for fsm " + fsm.id);
      resolve(process1);

    });
  });

}



class DebugFSM {
  /**
   * setting a single channel for all data
   * @param {*callback} breakpointReached 
   */
  static setBreakpointReachedCallback(breakpointReached) {
    _breakpointReached = breakpointReached;
  }

  static breakpointReached(bpReachData) {
    FSMManager = require('FSM/fsm-manager');
    //FSMManager.tickPause(bpReachData.processId);
    bpReachData.protocol = 'debugger';
    bpReachData.breakpointReached = true;
    _breakpointReached(bpReachData);
  }



  static stop(bpData) {
    return new Promise((resolve, reject) => {
      FSMManager = require('FSM/fsm-manager');
      // stop the tick
      FSMManager.tickStop(bpData.processId);
      return processModel.deleteProcess(bpData.processId);

    });
  }

  /**
   * continue
   * @param {*} fsmId 
   * not working!
   */
  static attach(bpData) {
    return new Promise((resolve, reject) => {

      fsmModel = require('models/fsmmodel');
      var fsm = fsmModel.getFSMSync(bpData.fsmId);
      if (!fsm) {
        return reject('WARNING: no fsm ' + fsmId);
      }
      // get the processes for this fsm
      processModel.get(bpData.processId).then((process) => {

        if (processes && processes.length) {
          _.each(processes, (process) => {
            FSMManager = require('FSM/fsm-manager');
            FSMManager.tickContinue(fsm.id, process);

          });
        }


      }).catch((ex) => {
        dblogger.error('cant attach to process ' + bpData.processId, ex);
        reject(ex);
      });

    });
  }
  /**
   * forward to the next node
   * @param {Object} bpData 
   */
  static step(bpData) {
    return new Promise((resolve, reject) => {

      dblogger.assert(bpData.processId, "must have pid");

      fsmModel = require('models/fsmmodel');
      var fsm = fsmModel.getFSMSync(bpData.fsmId);
      if (!fsm) {
        return reject('WARNING: no fsm ' + bpData.fsmId);
      }

      // get the processes for this fsm
      processModel.get(bpData.processId, fsm).then((process1) => {
        process1.volatile('exitBreakpoint', true);
        process1.volatile('stepping', true);
        process1.volatile('atABreakpoint', false);
      }).catch((ex) => { // if no process yet, wait until created
        dblogger.log('no process yet: ' + bpData.processId + "/" + ex.message + "/" + ex);
        startOneProcess(fsm, bpData).then((process1) => {
          process1.volatile('stepping', true);
        });
      });


    });
  }
  /**
   * continue
   * @param {*} fsmId 
   */
  static run(bpData) {
    return new Promise((resolve, reject) => {

      dblogger.assert(bpData.processId, "must have pid");

      fsmModel = require('models/fsmmodel');
      var fsm = fsmModel.getFSMSync(bpData.fsmId);
      if (!fsm) {
        return reject('WARNING: no fsm ' + bpData.fsmId);
      }
      // get the processes for this fsm
      processModel.get(bpData.processId, fsm).then((process1) => {
        process1.volatile('exitBreakpoint', true);
        process1.volatile('atABreakpoint', false);
        process1.volatile('stepping', false);
        resolve(process1);
      }).catch((ex) => {
        dblogger.log('no process yet: ' + bpData.processId + "/" + ex.message + "/" + ex);

        startOneProcess(fsm, bpData).then((p) => {
          p.volatile('stepping', false);
          resolve(p);
        });

      });

    });


  }

  /**
   * set a group of breakpoints
   * @param {string} fsmId 
   * @param {string} processId 
   * @param {Object} bpData 
   */
  static setAllBreakpoints(fsmId, processId, bpData) {
    return new Promise((resolve, reject) => {
      fsmModel = require('models/fsmmodel');
      var fsm = fsmModel.getFSMSync(fsmId);
      if (!fsm) {
        return reject('WARNING: no fsm ' + fsmId);
      }
      // get the processes for this fsm
      var processObj = processModel.getFromCache(processId);
      _.each(bpData, (breakpoint) => {
        processObj.setBreakpoint(breakpoint);
      });
    });

  }
  /**
   * 
   * @param {*} nodeId 
   */
  static setBreakpoint(bpData) {
    return new Promise((resolve, reject) => {

      // get the processes for this fsm
      processModel.getActiveFSMProcesses(bpData.fsmId).then((processes) => {

        if (processes && processes.length) {
          _.each(processes, (process) => {
            process.setBreakpoint(bpData);
          });
        }


      });

    });
  }

  /**
   * clear a bp for an FSM
   * @param {*} fsmId 
   * @param {*} nodeId 
   */
  static clearBreakpoint(bpData) {
    return new Promise((resolve, reject) => {
      // get the processes for this fsm
      processModel.getActiveFSMProcesses(bpData.fsmId).then((processes) => {

        if (processes && processes.length) {
          _.each(processes, (process) => {
            process.clearBreakpoint(bpData.nodeId);
          });
        }
      });

    });
  }


}

module.exports = DebugFSM;
