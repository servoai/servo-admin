var b3 = require('FSM/core/b3');
var Condition = require('FSM/core/condition');
var utils = require('utils/utils');
var dblogger = require('utils/dblogger');
var _ = require('underscore');

/**
 * Succeeds if the intent in the current target equals to one of the intentId from the parameters, fails otherwise
 * @memberof module:Conditions
 */
class IntentCondition extends Condition {

  constructor(settings) {
    super();

    this.title = this.name = 'IntentCondition';
    settings = settings || {};
    /**
     * Node parameters.
     *
     * @property parameters
     * @type {Object}
     * @property {(string|Array<string>)} parameters.intentId - an array of or a single intentId
     **/
    this.parameters = _.extend(this.parameters, {
      'intentId': ''
    });
    this.description = "Succeeds if the intent in the current target equals to one of the intentId from the parameters, fails otherwise"
    if (settings && utils.isEmpty(settings.intentId)) {
      console.error("intentId parameter in intentId condition is an obligatory parameter");
    }

  }

  /**
   * Tick method.
   *
   * @private tick
   * @param {Tick} tick A tick instance.
   * @return {Constant} A state constant.
   **/
  tick(tick) {

    // if retrieval command
    if (tick.target.getMessageObj() &&
      tick.target.getMessageObj().intentId) {
      this.properties.intentId = this.properties.intentId || this.properties.intentToTest;
      var intents = Array.isArray(this.properties.intentId) ?
        this.properties.intentId : [this.properties.intentId];
      var found = intents.find((elem) => {
        return tick.target.getMessageObj().intentId.toLowerCase() === elem.toLowerCase();
      })

      // we're done here
      return found ? b3.SUCCESS() : b3.FAILURE();
    } else return b3.FAILURE();

  }
}

module.exports = IntentCondition;
