var b3 = require('FSM/core/b3');
var Condition = require('FSM/core/condition');
var _ = require('underscore');
var utils = require('utils/utils');
var dblogger = require('utils/dblogger');


/**Compares between an entity (left side) and and a global/context/message/local/volatile
 * value (right side).
 * The left side is evaluated against the entity array at the message object and would usually
 * be an entity name. The right side is evaluated against the context data. If right side is a
 * string, wrap it in ''. It will return true if there is at least one entity value that satisfied the condition.
 * @memberof module:Conditions
 */
class EntityCondition extends Condition {

  constructor(settings) {
    super();
    this.title = this.name = 'EntityCondition';
    this.description = 'Compares between an entity (left side) and and a global/context/message' +
      " value (right side). " +
      "The left side is evaluated against the entity array at the message object and would usually" +
      "be an entity name. The right side is evaluated against the context data. If right side is a" +
      "string, wrap it in ''. It will return true if there is at least one entity value that satisfied the condition.";
    /**
     * Node parameters.
     *
     * @property parameters
     * @type {Object}
     * @prop {ExpressionString} parameters.left
     * @prop {Operator} parameters.operator - logical operator
     * @prop {CompositeFieldName} parameters.right - field name to evaluate against  
     *  
     **/
    this.parameters = {
      'left': '',
      'operator': '',
      'right': ''
    };

    settings = settings || {};
    if (utils.isEmpty(settings.left)) {
      console.error("left parameter in EntityCondition is an obligatory parameter");
    } else if (utils.isEmpty(settings.operator)) {
      console.error("operator parameter in EntityCondition is an obligatory parameter");
    } else if (utils.isEmpty(settings.right)) {
      console.error("right parameter in EntityCondition is an obligatory parameter");
    }
  }

  /**
   * Tick method.
   *
   * @private tick
   * @param {Tick} tick A tick instance.
   * @return {Constant} A state constant.
   **/
  tick(tick) {
    var data = this.alldata(tick);
    var entities = tick.target.getMessageObj().entities;
    var left = this.properties.left;
    var operator = this.properties.operator;
    var right = this.properties.right;

    left = utils.wrapExpression(left);
    right = utils.wrapExpression(right);
    try {
      left = _.template(left)(entities);
    } catch (ex) {
      dblogger.error('problem in templating left param at EntityCondition:', left)
      left = 0;
    }
    try {
      right = _.template(right)(data);
    } catch (ex) {
      dblogger.error('problem in templating right param at EntityCondition:', left)
      right = 0;
    }

    // fix a common error (especially for non-programmers)
    if (operator === '=') {
      operator = '==='
    }

    var result = false;

    left = utils.addQuotes(left);
    right = utils.addQuotes(right);
    if (_.isArray(left)) {
      var i = 0;
      while (i < left.length && !result) {
        result = eval(left[i] + operator + right);
        i++;
      }
    } else {
      result = eval(left + operator + right);
    }

    if (result) {
      return b3.SUCCESS();
    } else {
      return b3.FAILURE();
    }
  }
}

module.exports = EntityCondition;
