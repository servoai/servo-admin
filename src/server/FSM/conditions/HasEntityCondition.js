var b3 = require('FSM/core/b3');
var Condition = require('FSM/core/condition');
var _ = require('underscore');
var utils = require('utils/utils');
var dblogger = require('utils/dblogger');

/**
 * Returns success if the current target is carrying an entity (extracted from NLU), failure otherwise
 * @memberof module:Conditions
 */
class HasEntityCondition extends Condition {

  constructor(settings) {
    super();
    this.title = this.name = 'HasEntityCondition';
    this.description = 'Checks if an entity exist';

    settings = settings || {};
    if (utils.isEmpty(settings.name)) {
      console.error("name parameter in HasEntityCondition is an obligatory parameter");
    }
  }

  /**
   * Tick method.
   *
   * @private tick
   * @param {Tick} tick A tick instance.
   * @return {TickStatus} A state constant.
   **/
  tick(tick) {
    var entity = tick.target.getEntity(this.properties.name);

    if (!!entity) {
      return b3.SUCCESS();
    } else {
      return b3.FAILURE();
    }
  }
}

module.exports = HasEntityCondition;
