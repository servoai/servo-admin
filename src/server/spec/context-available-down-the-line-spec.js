let ProcessTick = require('./support/start-process-tick');


describe('load context tree', () => {
  var processTick;
  beforeEach((done) => {
    processTick = new ProcessTick();
    // get the tree
    let fsm = require('./spec-trees/context-available-down-the-line-tree.json');;
    // start the process  & tick
    return processTick.start(fsm).then(() => {

      done();
    });
  });

  it('ask a number question', (done) => {

    processTick.expect('how much?').then(() => {
      processTick.send("goto help").then(() => {
        processTick.expect('how much?').then(() => {
          processTick.send({
            entities: {
              number: 123
            }
          }).then(() => {

            processTick.expect('123 found').then(() => {
              done();
            }).catch((x) => {
              done.fail(x);
            });
          }).catch(() => {
            console.log('reject send 12');
          });
        }).catch(() => {
          console.log('howmuch2');
        });
      });

    }).catch(() => {
      console.log('howmuch1');
    });


  });




  afterEach(() => {
    processTick.stop();
  });

});
