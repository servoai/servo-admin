let StartProcessTick = require('./support/start-process-tick');
var fsmEventEmitter = require('FSM/fsm-event-emitter.js');
var results = {};
describe('load environment nodes', () => {
  var startProcessTick;
  beforeEach((done) => {
    startProcessTick = new StartProcessTick();
    // get the tree
    let fsm = require('./spec-trees/general-message-tree-spec.json');
    // start the process & tick
    startProcessTick.start(fsm);
    done();

  });

  it('a general message test', (done) => {
    startProcessTick.expect('test general message').then(() => {
      done();
    });
  });

  afterEach(() => {
    startProcessTick.stop();
  })

});
