let ProcessTick = require('./support/start-process-tick');

describe('fsm properties tree', () => {
  var processTick;
  beforeEach((done) => {
    processTick = new ProcessTick();
    // get the tree
    let fsm = require('./spec-trees/set-fsm-properties-tree.json');
    // start the process  & tick
    processTick.start(fsm).then(() => {
      done();
    });
  });

  it('fsm compare test', (done) => {

    processTick.expect('english enabled').then(() => {
      done()
    }).catch((x) => {
      processTick.fail(x, done);
    });

  });

  afterEach(() => {
    processTick.stop();
  });

});
