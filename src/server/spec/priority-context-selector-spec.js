let ProcessTick = require('./support/start-process-tick');


describe('priority context tree test', () => {
  var processTick;
  beforeEach((done) => {
    processTick = new ProcessTick();
    // get the tree
    let fsm = require('./spec-trees/priority-context-selector-spec.json');;
    // start the process  & tick
    processTick.start(fsm).then(() => {
      done();
    });
  });

  it('wire answer on same context', (done) => {
    processTick.expect('what would you like to do?').then(() => {
      processTick.send({
        useNLU: false,
        "intentId": "WireIntent"
      }).then(() => {
        processTick.expect('how much would you like to transfer?').then(() => {
          processTick.send({
            useNLU: false,
            entities: {
              'number': '1245'
            }
          }).then(() => {
            processTick.expect('transfer of $1245 scheduled').then(() => {
              done();
            }).catch((x) => {
              processTick.fail(x, done);
            });
          }).catch((x) => {
            processTick.fail(x, done);
          });
        }).catch((x) => {
          processTick.fail(x, done);
        });
      });
    });

  });

  it('balance answer on same context', (done) => {
    processTick.expect('what would you like to do?').then(() => {
      processTick.send({
        useNLU: false,
        "intentId": "BalanceIntent"
      }).then(() => {
        processTick.expect('your balance is $10000').then(() => {
          done();
        });
      });
    });
  });

  it('helper if no intent', (done) => {
    processTick.expect('what would you like to do?').then(() => {
      processTick.send({
        useNLU: false,
        "intentId": "WireIntent"
      }).then(() => {
        processTick.expect('how much would you like to transfer?').then(() => {
          processTick.send({
            useNLU: false
          }).then(() => {
            processTick.expect('what would you like to do?').then(() => {
              done();
            });
          });
        });
      });
    });
  });

  it('switch context', (done) => {
    processTick.expect('what would you like to do?').then(() => {
      processTick.send({
        useNLU: false,
        "intentId": "WireIntent"
      }).then(() => {
        processTick.expect('how much would you like to transfer?').then(() => {
          processTick.send({
            useNLU: false,
            "intentId": "BalanceIntent"
          }).then(() => {
            processTick.expect('your balance is $10000').then(() => {
              processTick.expect('how much would you like to transfer?').then(() => {
                done();
              });
            });
          });
        }).catch((x) => {
          processTick.fail(x, done);
        });
      });
    });
  });

  afterEach(() => {
    processTick.stop();
  });

});
