let ProcessTick = require('./support/start-process-tick');


describe('load environment nodes', () => {
  var processTick;
  beforeEach((done) => {
    processTick = new ProcessTick();
    // get the tree
    let fsm = require('./spec-trees/ask-in-context-tree-spec.json');;
    // start the process  & tick
    processTick.start(fsm).then(() => {
      done();
    });
  });

  it('ask as root node', (done) => {

    processTick.expect('ask yes or not?').then(() => {
      processTick.send('PositiveIntent').then(() => {
        processTick.expect('Yes!').then(() => {
          done();
        }).catch((x) => {
          done.fail(x);
        });
      }).catch(() => {
        console.log('reject1');
      });
    }).catch(() => {
      console.log('reject2');
    });


  });

  it('asks AFTER message received ', (done) => {

    processTick.send('PositiveIntent').then(() => {
      processTick.expect('ask yes or not?').then(() => {
        processTick.expect('Yes!').then(() => {
          processTick.send('None').then(() => {
            processTick.expect('I don\'t understand').then(() => {
              done();
            }).catch((x) => {
              done.fail(x);
            });
          }).catch(() => {
            console.log('reject1');
          });
        }).catch(() => {
          console.log('reject2');
        });
      }).catch(() => {
        console.log('reject3');
      });
    });


  });


  afterEach(() => {
    processTick.stop();
  });

});
