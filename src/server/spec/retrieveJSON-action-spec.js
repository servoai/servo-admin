let StartProcessTick = require('./support/start-process-tick');
var fsmEventEmitter = require('FSM/fsm-event-emitter.js');
var results = {};
describe('load environment nodes', () => {
  var startProcessTick;
  beforeEach((done) => {
    startProcessTick = new StartProcessTick();
    // get the tree
    let fsm = require('./spec-trees/retrieveJSON-action-spec.json');
    // start the process & tick
    startProcessTick.start(fsm);
    done();

  });

  it('a get action test', (done) => {
    startProcessTick.expect("get ok: \"http://httpbin.org/get\"").then(() => {
      done();
    });
  });

  afterEach(() => {
    startProcessTick.stop();
  })

});
