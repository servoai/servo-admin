let ProcessTick = require('./support/start-process-tick');


describe('load environment nodes', () => {
  var processTick;
  beforeEach((done) => {
    processTick = new ProcessTick();
    // get the tree
    let fsm = require('./spec-trees/ask-in-context-map-entity-tree-spec.json');;
    // start the process  & tick
    processTick.start(fsm).then(() => {
      done();
    });
  });

  it('map entity in ask in context test', (done) => {

    processTick.expect('ask yes or not with a number').then(() => {
      processTick.send({
        intentId: 'PositiveIntent',
        entities: {
          'number': 12121
        }
      }).then(() => {
        processTick.expect('Yes 12121!').then(() => {
          done();
        }).catch((x) => {
          done.fail(x);
        });
      }).catch(() => {
        console.log('reject1');
      });
    }).catch(() => {
      console.log('reject2');
    });


  });

  afterEach(() => {
    processTick.stop();
  });

});
