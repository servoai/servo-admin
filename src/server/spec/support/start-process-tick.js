// add this path to requires
require('app-module-path').addPath(__dirname + "/../..");

let chatManager = require('chat/chatmanager');
let FSM = require('FSM/fsm-manager');
let fsmModel = require('models/fsmmodel');
let MockupTicker = require('spec/support/mockup-ticker').getInst();
FSM.setTicker(MockupTicker);
const TESTPID = "test-processId";
var chatsim = require('chat/chatsim').getInst();
var fsmEventEmitter = require('FSM/fsm-event-emitter.js');
var _ = require('underscore');

console.log('_________USE FDESCRIBE FOR SINGLE SPEC RUNS_________')
let _observedObjects = [];
class Observer {
  constructor() {

  }
  static expect(cb) {
    this.cb = cb;
    let elemExpected;
    if (elemExpected = _observedObjects.pop()) {
      console.log('*********elem expected:', elemExpected);
      this.callback(elemExpected);
    }

  }

  static callback(obj) {

    let cb = this.cb;
    this.cb = null; // forget  the stack
    if (!cb) throw "no cb";
    cb(obj); // and call back
  }
  static set(obj) {

    if (this.cb) {
      //console.log('set obj, callback now', obj)
      this.callback(obj);
    } else { // if we havent set yet, save it
      //console.log('set obj, unshift to queue', obj)
      _observedObjects.unshift(obj);
    }

  }
}


fsmEventEmitter.on('messageSent', (obj) => {
  console.log('messageSent: ====>', obj);
  Observer.set(obj);
});

class StartProcessTick {
  constructor() {

    this._results = {};
  }

  start(fsm) {
    return new Promise((resolve) => {
      fsm.folderName = '../spec-trees/';
      fsmModel.addFsm(fsm);
      // load the tree
      //let behaviorTree = new BehaviorTree("general-message-spec", 'general-message-spec', "general-message-spec");
      //behaviorTree.load(tree).then((fsm) => {
      FSM.loadBehaviorTree(fsm, undefined /*this make it root*/ , fsm.id).then(() => {
        // create message object
        var messageObj = chatManager.createMessageObject('chatsim', {
          recipient: {
            id: TESTPID
          },
          sender: {
            id: TESTPID
          },
          text: "",
          treeID: fsm.id,
          raw: {}
        });

        // start the process
        FSM.startOneProcess(fsm, messageObj, TESTPID).then((process1) => {

          FSM.tickStart(fsm.id, process1);
          this.fsmId = fsm.id;
          resolve();
        });
      });
    });


  }

  results() {
    return this._results;
  }

  stop() {
    FSM.tickStop(TESTPID);
  }

  /**
   * 
   * @param {string|Object} dataObjOrIntentId 
   */
  send(intentObj) {
    if (typeof intentObj === 'string') {
      intentObj = {
        data: {
          useNLU: false,
          utterance: intentObj,
          intentId: intentObj,
          fsmId: this.fsmId,
          processId: TESTPID
        }
      };
    } else {
      let intentObj1 = {
        data: {
          useNLU: false,
          fsmId: this.fsmId,
          processId: TESTPID
        }
      };

      intentObj.data = _.extend(intentObj1.data, intentObj);
    }

    // send  
    return chatsim.onMessage(intentObj);
  }

  fail(err, done) {
    console.log('--------FAILURE-----------:', err);
    done.fail(err);
  }


  /**
   * 
   * @param {string|Object} dataObj 
   */
  expect(dataObj) {
    console.log('##### EXPECT ', dataObj);
    return new Promise((resolve) => {
      if (typeof dataObj === 'string') {
        Observer.expect((ev) => {
          console.log('####### RECEIVED ', ev, ' FOR EXPECTED ', dataObj);
          expect(dataObj.toLowerCase()).toEqual(ev.payload.toLowerCase());
          resolve();

        });
      }

    });
  }
}



module.exports = StartProcessTick;
