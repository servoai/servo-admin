const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs-extra'));
const FSMManager = require('../FSM/fsm-manager');
const fsmModel = require("../models/fsmmodel");
const dblogger = require('../utils/dblogger');
const _ = require("lodash");
const pathLib = require('path');

const CONVOCODE_DIR = "./convocode";

const getPathInfo = (path, projectsDir) => {
  path = path.replace(/\\/g, '/'); //- lior added but could be removed after Netanel 's fixes 09052018
  const sections = path.split('/');
  if (sections.length == 5) {
    sections.splice(0, 2);
  }
  if (sections.length == 3) {
    return {
      convocode: CONVOCODE_DIR,
      projectsDir,
      state: sections[0],
      tree: sections[1],
      filename: sections[2]
    };
  } else {
    return {
      filename: sections[sections.length - 1]
    };
  }
};

const getTreeDir = (pathObj) => {
  const path = [];
  path.push(CONVOCODE_DIR);
  path.push(pathObj.projectsDir);
  path.push(pathObj.state);
  path.push(pathObj.tree);
  return path.join("/");
};

const ensureProjectsDir = (dir) => {
  const userDir = CONVOCODE_DIR + "/" + dir;
  const commonDir = CONVOCODE_DIR + "/common";
  fs.ensureDir(userDir);
  fs.ensureDir(userDir + "/fsms");
  fs.ensureDir(userDir + "/drafts");
  if (!fs.existsSync(userDir + "/drafts/getting-started")) {
    fs.copySync(commonDir + "/getting-started", userDir + "/drafts/getting-started");
  }

};

const list = (req, res) => {
  const userDir = req.user.projectsDir;
  try {
    fsmModel.getAllFSMs(null, true, false).then(function (fsms) {
      setTimeout(function () {
        try {
          var rootFSMs = {};
          // Filter only user's fsms
          const userFSMs = _.pickBy(fsms, (fsm) => {
            return fsm.path.split(pathLib.posix.sep)[1] == userDir;
          });
          for (let id in userFSMs) {
            const fsm = userFSMs[id];
            if (!fsm.isRoot) {
              continue;
            }
            var clone = JSON.parse(JSON.stringify(fsm));
            fsm.trees = [];
            fsm.trees.push(clone);
            rootFSMs[id] = fsm;
          }
          for (let id in userFSMs) {
            const fsm = userFSMs[id];
            if (fsm.isRoot) {
              continue;
            }
            const pathArr = fsm.path.split(pathLib.posix.sep);
            const rootFSMid = pathArr[3];
	    if (!rootFSMid || !rootFSMs[rootFSMid]) {
		    dblogger.error('problem in path. unsupported folder structure:'+fsm.path);
	    } else {

            rootFSMs[rootFSMid].trees.unshift(fsm);
	    }
          }
          for (let rootFSMid in rootFSMs) {
            var last = rootFSMs[rootFSMid].trees.pop();
            rootFSMs[rootFSMid].trees.sort((a, b) => a.id.toLowerCase().localeCompare(b.id.toLowerCase()));
            rootFSMs[rootFSMid].trees.push(last);
          }
          return res.send(rootFSMs);
        } catch (ex) {
          return res.status(500).send(ex);
        }
      }, 100);
    }).catch(function (err) {
      return res.status(500).send(err);
    });
  } catch (ex) {
    return res.status(500).send(ex);
  }
};

const load = (req, res) => {
  try {
    var path = req.query.path;
    var dir = req.user.projectsDir;
    path = CONVOCODE_DIR + "/" + dir + "/" + path;

    if (!fs.existsSync(path)) {
      var err = "file wasnt found: " + path;
      dblogger.error(err);
      return res.status(500).send(err);
    }
    fs.readFile(path, 'utf8', (err, content) => {
      if (err) {
        dblogger.error(err);
        res.status(500).send("error in readfile:" + path + ":" + err.message);
      } else {
        res.send(content);
      }
    });
  } catch (ex) {
    return res.status(500).send(ex);
  }
};

const save = (req, res) => {
  try {
    dblogger.log('save requested:' + req.body.path);
    const content = req.body.data;
    const projectsDir = req.user.projectsDir;
    const path = getPathInfo(req.body.path, projectsDir);

    const project = JSON.parse(content);
    if (project.data && project.data.trees) {
      dblogger.log('trees number:' + project.data.trees.length);
      const rootDir = getTreeDir(path);
      fs.ensureDirSync(rootDir);

      for (let i = 0; i < project.data.trees.length; i++) {
        let tree = project.data.trees[i];
        if (Object.keys(tree.nodes).length == 0) {
          continue;
        }

        tree = _.omit(tree, ["lastPublishPublished", "lastPublished", "lastSaved", "isRoot", "path", "folderName"]);
        tree.state = "draft";

        const treeString = JSON.stringify(tree, null, '\t');
        // If a subtree or a root tree
        if (tree.id != path.tree) {
          fs.ensureDirSync(rootDir + "/trees/");
          fs.ensureDirSync(rootDir + "/trees/" + tree.id);
          fs.writeFile(rootDir + "/trees/" + tree.id + "/" + tree.id + ".json", treeString, {
            flag: 'w'
          });
        } else {
          fs.writeFile(rootDir + "/" + tree.id + ".json", treeString, {
            flag: 'w'
          });
        }
      }

      // now remove whats not there
      fs.readdir(rootDir + "/trees", function (err, treeDirs) {
        if (err) {
          // No subtrees exists
          return res.send({
            "result": true
          });
        }
        for (let i = 0; i < treeDirs.length; i++) {
          const found = project.data.trees.find((tree) => tree.id === treeDirs[i]);

          // if a file on the folder does not exist on the data to be saved, remove it
          if (!found) {
            fs.removeSync(rootDir + "/trees/" + treeDirs[i]);
          }
        }

        res.send({
          "result": true
        });
      });
    } else {
      // If a simple file
      fs.writeFile(CONVOCODE_DIR + "/" + projectsDir + "/" + path.filename, content, {
        flag: 'w'
      });
      res.send({
        "result": true
      });
    }
  } catch (err) {
    dblogger.error('error in save', err);
    res.status(500).send(err);
  }
};

const publish = (req, res) => {
  try {
    dblogger.info('publish start for ' + req.body.name);
    var dir = req.user.projectsDir;
    var path = CONVOCODE_DIR + "/" + dir + "/drafts/" + req.body.name;
    var dst = path.replace("drafts", "fsms");
    fs.removeSync(dst);
    fs.copyAsync(path, dst).then(() => {
      FSMManager.resetBehaviorTrees(req.body.name);
      dblogger.info('publish end for ' + req.body.name);
      res.send({
        "result": true
      });
    }).catch((err) => {
      dblogger.error('error in publish', err);
      res.status(500).send(err);
    });
  } catch (ex) {
    return res.status(500).send(ex);
  }
};

const remove = (req, res) => {
  try {
    var dir = req.user.projectsDir;
    var path = CONVOCODE_DIR + "/" + dir + "/" + req.query.path;
    var pathArr = path.split(pathLib.posix.sep);
    pathArr.splice(-1, 1);
    path = pathArr.join("/");
    fs.remove(path).then(() => {
      res.send({
        "result": true
      });
    }).catch((err) => {
      dblogger.error(err);
      res.status(500).send(err);
    });
  } catch (ex) {
    return res.status(500).send(ex);
  }
};

module.exports = {
  ensureProjectsDir,
  remove,
  list,
  save,
  load,
  publish
};
