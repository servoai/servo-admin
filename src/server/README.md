
*Servo Bot Automation Framework* uses the Behavior Tree paradigm to enable developers to develop bots
and automation using a visual architecture, open code and reusable AI.

Among other things, it features:
* A behavior tree editor
* A debugger
* Reusable subtrees
* Drivers for Alexa and Facebook clients 
* Drivers for MongoDB and Couchbase datastores
* Context Dection, Entity mapping an Continuous learning  
* Human chat behavior built-in - eg automated repeats, conversation quote-per day, greetings and more.
* Easy extention with NodeJS ES6 classes

It is built for developers, by developers.

## Tutorials

Following are some general behavior tree tutorials as well as Servo's specific guides 
<ul> 
<li><a href="https://en.wikipedia.org/wiki/Behavior_tree_(artificial_intelligence,_robotics_and_control)" target="_blank"> Wikipedia's Behavior Tree article </a> </li>
<li><a href="https://www.gamasutra.com/blogs/ChrisSimpson/20140717/221339/Behavior_trees_for_AI_How_they_work.php" target="_blank"> Behavior trees for AI: How they work</a></li>
<li><a href="http://aigamedev.com/open/article/bt-overview/" target="_blank">Understanding Behavior Trees</a></li>
<li><a href="tutorials/Servo-alexa-tutorial.html" target="_blank"> Getting Started with Servo for Alexa</a></li>
<li><a href="tutorials/TheFSM.html" target="_blank"> The FSM and its properties: a quick reference</a></li>
</ul>


 