import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ConfigParams {
  params:any;

   getParams():Observable<boolean> {
            // return immediately if already brought the params

            return new Observable<boolean> ((observer:any) => {
                if (!this.params) {
                    this.http.get('/configparams.json')
                        // Call map on the response observable to get the parsed  object
                        .map(function (res:any) {
                            return res.json();
                        })
                        // Subscribe to the observable to get the parsed  object and attach it to the
                        .subscribe((params:any) => {
                            this.params = params;
                            observer.next(true);
                            observer.complete();
                        });
                } else {
                    observer.next(true);
                    observer.complete();
                }
           });
   }

  get(key:string): any {
    return this.params.key;
  }

  add(key: string,value: string): void {
    this.params[key] = (value);
  }

  getDataHost(): string {
      return this.params.datahost;
  }

  getDataPort(): string {
      return this.params.dataport;
  }

  getDataUrl(): string {
      var x:string = this.getDataHost() + ':' + this.getDataPort();
      // append /
      if (x.charAt(x.length) !== '/') {
          x += '/';
      }
      return x;
  }


    /**
     * service to bring in params
     * @param http
     */
  constructor(public http:Http) {

  }
}
