import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, Routes, Router } from '@angular/router';
import { HTTP_PROVIDERS} from '@angular/http';

import {HomePage} from './components/home/home';
import {DocumentationPage} from './components/documentation/documentation';
import {FormPage} from './components/form/form';
import {AuthPage} from './components/auth/auth';
import {ProcessPage} from './components/process/process';
import {UsersPage} from './components/users/users';
import {ButtonsPage} from './components/buttons/buttons';
import {NotificationsPage} from './components/notifications/notifications';
import {TypographyPage} from './components/typography/typography';
import {IconsPage} from './components/icons/icons';
import {GridPage} from './components/grid/grid';
import {PanelsWellsPage} from './components/panels-wells/panels-wells';
import {ConfigParams} from './services/configparams';

/**
 * This class represents the main application component. Within the @Routes annotation is the configuration of the
 * applications routes, configuring the paths for the lazy loaded components (HomeComponent, AboutComponent).
 */
@Component({
 // moduleId: module.id,
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  viewProviders: [HTTP_PROVIDERS],
  templateUrl: 'app/app.component.html',
    styleUrls:['app/sb-admin-2.css'],
  directives: [ROUTER_DIRECTIVES, HomePage],
  providers:[ConfigParams]
})
@Routes([
  {
    path: '/home',
    component: HomePage
  },
  { 
    path: '/documentation', 
    component: DocumentationPage  
  },  
  { 
    path: '/form', 
    component: FormPage
  },
  { 
    path: '/login', 
    component: AuthPage
  },
  { 
    path: '/users', 
    component: UsersPage
  },
  { 
    path: '/process', 
    component: ProcessPage
  },
  { 
    path: '/auth', 
    component: AuthPage
  }
  /*,
  { 
    path: '/panels-wells', 
    component: PanelsWellsPage
  },
  { 
    path: '/buttons', 
    component: ButtonsPage
  },
  { 
    path: '/notifications', 
    component: NotificationsPage 
  },
  { 
    path: '/typography', 
    component: TypographyPage
  },
  {     
    path: '/icons', 
    component: IconsPage
  },
  { 
    path: '/grid', 
    component: GridPage
  }*/
])

export class AppComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {
    this.router.navigate(['/home']);
  }
}
