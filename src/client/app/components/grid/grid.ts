import {Component} from '@angular/core';
import {WrapperCmp} from '../header/header';

@Component({
  selector: 'grid',
  templateUrl: '../app/components/grid/grid.html',
  styleUrls: ['../app/components/grid/grid.css'],
  directives: [WrapperCmp]
})
export class GridPage {
}
