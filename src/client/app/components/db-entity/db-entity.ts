import {Component, Input, ElementRef, EventEmitter} from '@angular/core';
import {CORE_DIRECTIVES, NgIf} from '@angular/common';
import {WrapperCmp} from '../header/header';
import {Http, Headers} from '@angular/http';
import {ConfigParams} from '../../services/configparams';
import {Markdown} from '../markdown/markdown';

class Guid {
    static newGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
/**
 * return HTML with syntax highlighting
 * @param json
 * @returns {string}
 */
function syntaxHighlight(json:any) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
        function(match:any) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
}

@Component({
    selector: 'db-entity',
    templateUrl: '../app/components/db-entity/db-entity.html',
    directives: [WrapperCmp, CORE_DIRECTIVES, NgIf, Markdown],
    outputs: [
        'entitysaved',
        'dbEntitySelected: dbentity_selected'
    ]
})

export class DbEntity {
    @Input() dbname: string;
    @Input()
    public set processId(process_id: string) {
        if (typeof process_id === 'undefined' || process_id.length === 0) {
            return;
        }

        this.selectedDbEntityId = process_id;
        this.getSelectedDbEntity();
    }


    public entitysaved: EventEmitter<string> = new EventEmitter<string>();
    public dbEntitySelected: EventEmitter<string> = new EventEmitter<string>();

    saveEditor: boolean = false;

    dbEntity: string;
    jsonDbEntity: any;

    dbEntityIds: Array<string>;


    selectedDbEntityId: string;
    selectedDbEntityName: string;
    selectedDbEntityFunction: string = '';
    editMode: boolean = false;
    addNewMode: boolean;
    message: string;

    /***
     * select handler
     * @param ev
     */
    onDbEntityIdSelected(ev:any) {
        console.log('onDbEntityIdSelected', ev.currentTarget.value);
        if (ev.currentTarget.value === 'Add New...') {
            this.dbEntitySelected.next('');
            this.addNewDbEntity();
        } else {
            this.selectedDbEntityId = ev.currentTarget.value;
            this.dbEntitySelected.next(this.selectedDbEntityId);

            this.getSelectedDbEntity();
        }

    }

    processIdSelected(process_id: string) {
        alert(process_id);
    }

    /***
     * get all the DbEntitys from the server
     */
    getAllDbEntitys() {
        this.configParams.getParams().subscribe(() => {
            this.http.get(this.configParams.getDataUrl() + 'apidb/getAllDbEntitys?type=' + this.dbname)
                // Call map on the response observable to get the parsed  object
                .map(function(res: any) {
                    return res.json();
                })
                // Subscribe to the observable to get the parsed  object and attach it to the
                .subscribe((dbEntitys: any) => {

                    this.dbEntityIds = [];
                    // if the doc exists
                    if (dbEntitys.code !== 13 || dbEntitys.message !== 'The key does not exist on the server') {
                        this.dbEntityIds = dbEntitys.map((obj:any) => {
                            return obj.id;
                        });
                    }
                    this.dbEntityIds.push('Add New...');
                    console.log('get all DbEntitys', dbEntitys, this.dbEntityIds);
                    // simulate the $event sent on a click
                    this.onDbEntityIdSelected({ currentTarget: { value: (this.selectedDbEntityId || this.dbEntityIds[0]) } });
                }
                );
        });


    }

    /***
     *  format with colors the JSON
     */
    showFormatted() {
        var domEl: any = $(this.elementRef.nativeElement);
        domEl.find('#jsonDbEntity').
            html(syntaxHighlight(this.selectedDbEntityFunction));
    }
    /***
     * get the selected DbEntity into DbEntity var
     */
    getSelectedDbEntity() {
        this.configParams.getParams().subscribe(() => {
            this.http.get(this.configParams.getDataUrl() +
                'apidb/getDbEntity?type=' + this.dbname +
                '&dbEntity_id=' + this.selectedDbEntityId)
                .map(function(res: any) {
                    return res.json();
                })
                .subscribe(dbEntity => {
                    this.jsonDbEntity = dbEntity;
                    this.dbEntity = JSON.stringify(dbEntity, null, 4);
                    this.selectedDbEntityId = dbEntity.id;
                    this.selectedDbEntityName = dbEntity.name;
                    this.selectedDbEntityFunction = dbEntity.function;
                    this.showFormatted();
                }
                );
        });
    }


    /**
     * add a new DbEntity
     */
    addNewDbEntity() {
        // set mode
        this.addNewMode = true;
		this.editMode = true;
        // get new uuid
        this.selectedDbEntityId = Guid.newGuid();
        this.selectedDbEntityName = '';
        this.selectedDbEntityFunction = '';
        // set entity empty
        this.dbEntity = 'Add a JSON Entity here';
    }

    /**
     * set edit mode
     * @param b
     */
    setEditMode(b: boolean) {
        this.editMode = b;
    }

    dbEntityChanged() {
        try {
            // reset message
            this.message = '';
            //var allowNewlinesDbEntity = this.dbEntity.replace(/".*\n"/,function(x) {return x.replace('\n',' ')});
            // try to parse
            JSON.parse(this.dbEntity);

        } catch (e1) {
            // problem with parse
            this.message = 'not a valid JSON:' + e1.toString();
        }
    }

    deleteEntity() {
        if (!confirm('Are you sure?')) {
            return;
        }

        // a json object
        var DbEntity: any = {};

        // overwrite the id if anyone had any thoughts
        DbEntity.id = this.selectedDbEntityId;
        // wrap with type details
        var MetaDbEntity = {
            type: this.dbname,
            entity: DbEntity
        };
        // now post
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.configParams.getParams().subscribe(() => {
            this.http.post(this.configParams.getDataUrl() + 'apidb/deleteDbEntity',
                JSON.stringify(MetaDbEntity), {
                    headers: headers
                }).subscribe((resp: any) => {

                    this.message = this.selectedDbEntityId + 'Deleted';

                    console.log('delete DbEntity response', resp);

                    this.selectedDbEntityId = null;
                    this.getAllDbEntitys();
                    this.entitysaved.next(this.selectedDbEntityId);
                },
                err => console.error('error in upsertDbEntity', err)
                );
        });


    }

    /**
     * Tells Editor to save changes
     */
    saveEditorContent() {
        this.saveEditor = true;
    }

    /**
     * ACE Editor Save handler
     */
    saveHandler(editorContent: string) {
        this.selectedDbEntityFunction = editorContent;
        this.saveEditor = false;
        this.saveDbEntity();
    }

    /***
     * save the DbEntity
     */
    saveDbEntity() {
        // a json object
        var DbEntity:any;
        try {
            DbEntity = JSON.parse('{}');
        } catch (e1) {
            // problem with parse
            alert('not a valid JSON. Please confirm at www.jsoneditoronline.org:' + e1.toString());
        }

        // overwrite the id if anyone had any thoughts
        DbEntity.id = this.selectedDbEntityId;
        DbEntity.name = this.selectedDbEntityName;
        DbEntity.function = this.selectedDbEntityFunction;

        // wrap with type details
        var MetaDbEntity = {
            type: this.dbname,
            entity: DbEntity
        };
        // now post
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.configParams.getParams().subscribe(() => {
            this.http.post(this.configParams.getDataUrl() + 'apidb/upsertDbEntity',
                JSON.stringify(MetaDbEntity), {
                    headers: headers
                }).subscribe((resp: any) => {

                    this.message = 'Saved';

                    console.log('save DbEntity response', resp);

                    // add the newly added
                    if (this.addNewMode) {
                        // update the dropdown with a new DbEntity
                        this.dbEntityIds.pop();
                        this.dbEntityIds.push(this.selectedDbEntityId);
                        this.dbEntityIds.push('Add New...');
                    }

                    // set to view mode again
                    this.editMode = this.addNewMode = false;
                    this.getAllDbEntitys();
                    this.entitysaved.next(this.selectedDbEntityId);
                },
                err => console.error('error in upsertDbEntity', err)
                );
        });

    }

    /**
     * cancel
     */
    cancelEdit() {
        // set to view mode again
        this.editMode = false;
        this.addNewMode = false;

        // if we cancel out of addNew mode, the selectedDbEntityId isnt valid
        this.selectedDbEntityId = this.jsonDbEntity.id;
        this.selectedDbEntityName = this.jsonDbEntity.name;
        this.selectedDbEntityFunction = this.jsonDbEntity.function;

    }
    /***
     * ctor
     * @param http
     */
    constructor(public configParams: ConfigParams, public http: Http, public elementRef: ElementRef) {
        setTimeout(() => {
            // now, get all the entities already here
            this.getAllDbEntitys();
            // set the entity empty
            this.dbEntity = '';
        }, 200);

    }

}

