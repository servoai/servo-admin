/// <reference path="../../../../../typings/main.d.ts" />
/// <reference path="../../../../../typings/main/definitions/jointjs/jointjs.d.ts" />
import {Component, ElementRef,Input} from '@angular/core';
import {CORE_DIRECTIVES, NgIf} from '@angular/common';
interface State {
    name: string;
    intents: any;
}
interface Fsm {
    states? :State[];
};

interface Vertex{
	x:number;
	y:number;
}
@Component({
    selector: 'fsm-display',
    templateUrl: '../app/components/process/fsm-display.html',
    properties: ['title']

})
export class FsmDisplay {
    title: string;
    graph: joint.dia.Graph;
    _fsm:Fsm;
    /***
     * create a state at x,y,label
     */
    state(x:number, y:number, label:string):joint.dia.Cell {

        var cell = new joint.shapes.fsa.State({
            position: { x: x, y: y },
            size: { width: 60, height: 60 },
            attrs: {
                text: { text: label, fill: '#000000', 'font-weight': 'normal' },
                'circle': {
                    fill: '#f6f6f6',
                    stroke: '#000000',
                    'stroke-width': 2
                }
            }
        });
        this.graph.addCell(cell);
        return cell;
    }

    /**
     * link two states
     */
    link(source: any, target: any, label: string, vertices?: any[]) {

        if (!source || !target) {
            return null;
         }
        var cell = new joint.shapes.fsa.Arrow({
            source: { id: source.id },
            target: { id: target.id },
            labels: [{ position: 0.5, attrs: { text: { text: label || '', 'font-weight': 'bold' } } }],
            vertices: vertices || []
        });
        this.graph.addCell(cell);
        return cell;
    }

    createFsm(canvasW:number,canvasH:number) {

        var radius:number = 200;
        var maxStatesPerCircle = 7;
        var angleDelta = 2*Math.PI/maxStatesPerCircle;
        var curAngle = 0;
        var cells:joint.dia.Cell[] = [];
        var angles:number[] = [];

        function findCell(states:State[], nameObj:any) {
            var inx = states.findIndex((state:any)=>{
                if (typeof nameObj === "string") {
                    return state.name===nameObj;
                }else
                {
                    return state.name===nameObj.state;
                }


            });
            return inx;
        }
        function randomizeMiddleVertex(fromCell:joint.dia.Cell,toCell:joint.dia.Cell,
                                       fromInx:number,toInx:number):Vertex[] {
            var angleFrom = angles[fromInx];
            var angleTo = angles[toInx];
            var r = radius+80;

            return [];
        }

		function calcLoopVertex(toCell:joint.dia.Cell,inx:number):Vertex[] {

            var angle = angles[inx];
			var r = radius+80;
			var vx = r*Math.cos(angle+0.1) + canvasW/2;
			
			var vy = canvasH/2 - r*Math.sin(0.1+angle) ;
			
			var vx1 = r*Math.cos(angle-0.1) + canvasW/2;
			
			var vy1 = canvasH/2 - r*Math.sin(-0.1+angle);
			
			return [{x:vx,y:vy},{x:vx1,y:vy1}];
			
			
		}
			
        if (!this._fsm.states)
            return;
		if (!this.graph)
			return;
        this.graph.clear();
        
        for (var i:number=0;i<this._fsm.states.length;i++) {
            var state:State = this._fsm.states[i];
            var rx = Math.cos(curAngle) * radius;
            var ry = Math.sin(curAngle) * radius;
            var cx = canvasW/2 + rx;// 100 ==> 800/2 + 100 = 500
            var cy = canvasH/2 - ry;// 100 ==> 600/2 - 100 = 20

            // add the cell
            cells.push(this.state(cx,cy, state.name.replace(" ","\n").replace("-","\n")));
            angles.push(curAngle);
            curAngle += angleDelta;

            if (i && i%maxStatesPerCircle==0) {
                radius += 100;
            }
        };

        // for every state
        for (var i:number=0;i<this._fsm.states.length;i++) {
            var state:State = this._fsm.states[i];
            var fromCell:joint.dia.Cell = cells[i];
            // for every intent
            for (var intent in state.intents) {
                // find the cell that has the name of the intent
                var inx = findCell(this._fsm.states,state.intents[intent]);
                var toCell:joint.dia.Cell = cells[inx];
					var vertex:Vertex[] = undefined;
                if (toCell && toCell.id ===fromCell.id ) {
					vertex = calcLoopVertex(toCell,inx);
				} else
                {
                    vertex = randomizeMiddleVertex(fromCell,toCell,i,inx);
                }
                // and link to
                this.link(fromCell,toCell,intent.replace(" ","\n").replace("-","\n"),vertex);
            }

        }

    }

    @Input()
    set fsmjson(fsmJ: Fsm) {
        this._fsm = fsmJ ? fsmJ : {};
        this.createFsm(800,600);
    }

    /**
     * ctor
     * @param elementRef
     */
    constructor(private elementRef: ElementRef) {
        var domEl: any = $(this.elementRef.nativeElement);

        setTimeout(() => {
            // if already there, dont add more
            if (domEl.find('svg').length) {
                return;
            }

            this.graph = new joint.dia.Graph();

            var paperObj = {
                el: domEl,
                width: 800,
                height: 600,
                gridSize: 1,
                model: this.graph
            };
            var paper = new joint.dia.Paper(paperObj);
            var start = new joint.shapes.fsa.StartState({ position: { x: 50, y: 530 } });
            this.graph.addCell(start);


        },0);

    }
};
