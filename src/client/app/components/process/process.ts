/// <reference path="../../../../../typings/main.d.ts" />
/// <reference path="../../../../../typings/main/definitions/jointjs/jointjs.d.ts" />
import {Component, ElementRef} from '@angular/core';
import {CORE_DIRECTIVES, NgIf} from '@angular/common'
import {WrapperCmp} from '../header/header';
import {Http, Headers} from '@angular/http';
import {AgGridNg2} from 'ag-grid-ng2/main'
import {GridOptions} from 'ag-grid/main';
import {TAB_DIRECTIVES, AlertComponent} from 'ng2-bootstrap/ng2-bootstrap';
import {Processor} from '../processor/processor';
import {ConfigParams} from '../../services/configparams';
import {FsmDisplay} from './fsm-display';
declare var jQuery:JQueryStatic;
/**
 * return HTML with syntax highlighting
 * @param json
 * @returns {string}
 */
function syntaxHighlight(json:any) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
        function(match:any) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
}

@Component({
    selector: 'process-page',
    templateUrl: '../app/components/process/process.html',
    styleUrls: ['../app/components/process/process.css'],
    directives: [WrapperCmp, FsmDisplay, CORE_DIRECTIVES, NgIf, TAB_DIRECTIVES, AlertComponent, Processor, AgGridNg2]
})
export class ProcessPage {

    logTotalPages: number;
    logPageIndex: number;
    logrows: Array<any>;
    logPageSize: number;
    FSM: string;
    linkedFSM: string;
    fsm: any;
    fsmjson:any = {};
    title: string;
    fsmIds: Array<string>;
    description: string;
    selectedFsmId: string;
    editMode: boolean;
    addNewMode: boolean;
    message: string;
    maxId: number;
    tabProcessActive: boolean = false;

    processId: string;
    
    private gridOptions: GridOptions;
    private showGrid: boolean;
    private rowData: any[]=[];
    private gridColumnDefs: any[];
    private rowCount: string;
    private rowModelType: string;
    private pageSize: number;
    private pageIndex: number;
    private startRow: number;
    private endRow: number;

    /***
     * createColumnDefs()
     * create column for grid
     */
    private createColumnDefs(elWidth:number) {
        
        var colWidth = elWidth - 150;
        var templateText = function(params:any)   {
           return  '<div style="">' + JSON.stringify(params.value) + '</div>';
        };
        
        function calcDivHeight(params:any) {
		var html = JSON.stringify(params.node.data.text).replace(/\ /g, "&nbsp;");
                html = html.replace(/\</g, "&lt;");
                html = html.replace(/\>/g, "&gt;");
                var divStr = '<div style="width=500px;visibility:hidden;position:absolute;">' + 
				html
				+ '</div>';
		var domEl:any = $(divStr);
                var de = jQuery("body").append(domEl);
                var h = domEl.height();
                domEl.remove();
                if (h < 28) {
                    h = 28;
                }
            return h;
	}
				

    this.gridOptions.rowHeight = undefined;
    this.gridOptions.getRowHeight = function(params:any) {
        if (params.node.floating) {
            return 30;
        } else {
            return calcDivHeight(params);
        }
    }

    this.gridColumnDefs = [
            {
                headerName: 'Time', field: 'time',
                filter: 'text',width:120, filterParams: { apply: true, newRowsAction: 'keep' }
            },
            {
                headerName: 'Type', field: 'logType',
                filter: 'text',width:70, filterParams: { apply: true, newRowsAction: 'keep' }
            },
            {
                headerName: 'Text', field: 'text', width: colWidth,
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' },
                cellRenderer: function(params: any) {
                    var html = JSON.stringify(params.value).replace(/\ /g, "&nbsp;");
                    html = html.replace(/\</g, "&lt;");
                    html = html.replace(/\>/g, "&gt;");
                    return '<span title='+html+'>'+html+'</span>';
                }
            },
            {
                headerName: '', field: 'copy', width: 60,
                cellRenderer: function(params:any)  {
                    var html = JSON.stringify(params.value);//.replace(/\ /g, "&nbsp;");
                    console.log(params,'<---------------------')
                    // html = html.replace(/\</g, "&lt;");
                    // html = html.replace(/\>/g, "&gt;");
                    html= html.replace(/'/g,"\\'");
                    html= html.replace(/"/g,"\\'");
                    html = "'" + html + "'"
                    return '<button onclick="window.prompt(\'Copy to clipboard: Ctrl+C, Enter\',' + html + ');">copy</button>';
                }
            }
        ]
    
    }

    
    private calculateRowCount() {
        if (this.gridOptions.api && this.rowData) {
            var model = this.gridOptions.api.getModel();
            var totalRows = this.rowData.length;
            var processedRows = model.getRowCount();
            this.rowCount = processedRows.toLocaleString() + ' / ' + totalRows.toLocaleString();
        }
    }

    private onModelUpdated() {
        console.log('onModelUpdated');
        this.gridOptions.api.sizeColumnsToFit();
    }
    private onReady() {
        console.log('onReady');
        this.gridOptions.api.sizeColumnsToFit();
    }
    private getGridRows(params:any){
        // this code should contact the server for rows. however for the purposes of the demo,
            // the data is generated locally, a timer is used to give the experience of
            // an asynchronous call
            console.log('asking for ' + params.startRow + ' to ' + params.endRow);
            var servPageNum = Math.floor(params.endRow/this.logPageSize);
            console.log('Server Page : '+servPageNum);
            if (servPageNum >= 1) {
                if (params.endRow === servPageNum * this.logPageSize) {
                   this.getNextLogRows(params);
                   return;
                }
            }
            
            this.startRow = params.startRow;
            this.endRow = params.endRow;
            var rowsThisPage = this.rowData.slice(params.startRow, params.endRow);
            // see if we have come to the last page. if we have, set lastRow to
            // the very last row of the last page. if you are getting data from
            // a server, lastRow could be returned separately if the lastRow
            // is not in the current page.
            var lastRow = -1;
            if (this.rowData.length <= params.endRow) {
                lastRow = this.rowData.length;
            }
            params.successCallback(rowsThisPage, lastRow);
    }
    
    private createNewDatasource() {
        if (!this.rowData) {
            // in case user selected 'onPageSizeChanged()' before the json was loaded
            return;
        }
        var dataSource = {
            //rowCount: ???, - not setting the row count, infinite paging will be used
            pageSize: this.pageSize, // changing to number, as scope keeps it as a string
            getRows: (params:any) => {
                this.getGridRows(params);
            }
        };

        this.gridOptions.api.setDatasource(dataSource);
    }
    /***
     * select handler
     * @param ev
     */
    onFsmIdSelected(ev:any) {
        console.log('onFsmIdSelected', ev.currentTarget.value);
        if (ev.currentTarget.value === 'Add New...') {
            this.addNewFsm();
        } else {
            this.selectedFsmId = ev.currentTarget.value;
            this.getSelectedFsm();
        }
    }
    pageDown() {
        Math.max(--this.logPageIndex, 0);
        this.getAllLogRows();
    }

    pageUp() {
        Math.min(++this.logPageIndex, this.logTotalPages - 1);
        this.getAllLogRows();
    }

    /***
     * get all the DbEntitys from the server
     */
    getAllLogRows() {
        var domEl: any = $(this.elementRef.nativeElement);
        var logsTab = domEl.find('#logsTab');
        this.createColumnDefs(logsTab.width());
                        
        this.configParams.getParams().subscribe(() => {
            var apiURL = this.configParams.getDataUrl() + 'api/getLogs?' +
                'pageIndex=' +
                this.logPageIndex + '&pageSize=' + this.logPageSize;
            console.log('API URL : ' + apiURL);
            
            this.http.get(this.configParams.getDataUrl() + 'api/getLogs?' +
                'pageIndex=' +
                this.logPageIndex + '&pageSize=' + this.logPageSize)
                // Call map on the response observable to get the parsed  object
                .map(function(res: any) {
                    return res.json();
                })
                // Subscribe to the observable to get the parsed  object and attach it to the
                .subscribe((dbEntitys: any) => {
                    this.logrows = dbEntitys;
                    
                    for (var log of this.logrows) {
                        this.rowData.push({time:this.printDate(log.key),
                                    logType:log.value.logtype,
                                    text:log.value.text,
                                    copy:log.value.text});
                    }
                    
                    this.createNewDatasource();
                }
                );
            this.logTotalPages = 100;
        });
    }
    /***
     * get all the DbEntitys from the server
     */
    getNextLogRows(params:any) {

        this.configParams.getParams().subscribe(() => {
            var apiURL = this.configParams.getDataUrl() + 'api/getLogs?' +
                'pageIndex=' +
                params.endRow + '&pageSize=' + this.logPageSize;
            console.log('API URL : ' + apiURL);
            
            this.http.get(this.configParams.getDataUrl() + 'api/getLogs?' +
                'pageIndex=' +
                params.endRow + '&pageSize=' + this.logPageSize)
                // Call map on the response observable to get the parsed  object
                .map(function(res: any) {
                    return res.json();
                })
                // Subscribe to the observable to get the parsed  object and attach it to the
                .subscribe((dbEntitys: any) => {
                    //this.logrows = dbEntitys;
                    
                    for (var log of dbEntitys) {
                        this.rowData.push({time:this.printDate(log.key),
                                    logType:log.value.logtype,
                                    text:log.value.text,
                                    copy:log.value.text});
                    }
                    
                    this.startRow = params.startRow;
                    this.endRow = params.endRow;
                    var rowsThisPage = this.rowData.slice(params.startRow, params.endRow);
                    // see if we have come to the last page. if we have, set lastRow to
                    // the very last row of the last page. if you are getting data from
                    // a server, lastRow could be returned separately if the lastRow
                    // is not in the current page.
                    var lastRow = -1;
                    if (this.rowData.length <= params.endRow) {
                        lastRow = this.rowData.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }
                );
        });
    }
    /***
     * get all the fsms from the server
     */
    getAllFsms() {
        this.configParams.getParams().subscribe(() => {
            this.http.get(this.configParams.getDataUrl() +
                'api/getAllFSMs') // Call map on the response observable to get the parsed  object
                .map(function(res: any) {
                    return res.json();
                })
                // Subscribe to the observable to get the parsed  object and attach it to the
                .subscribe(fsms => {
                    this.maxId = -1;
                    // the server converts to an array
                    this.fsmIds = fsms.map((obj:any) => {
                        this.maxId = Math.max(this.maxId, parseInt(obj.id));
                        return obj.id;
                    });
                    this.fsmIds.push('Add New...');
                    console.log('get all fsms', fsms, this.fsmIds);
                    // simulate the $event sent on a click
                    this.onFsmIdSelected({ currentTarget: { value: this.selectedFsmId || this.fsmIds[0].toString() } });
                }
                );
        });

    }

    processSelected(process_id: string) {
        this.processId = process_id;
    }

    /***
     * format with colors the JSON
     */
    showFormatted() {
        // TODO: USE RENDERER
        var domEl: any = $(this.elementRef.nativeElement);
        var _this = this;

        domEl.find('#jsonFSM').
            html(this.linkedFSM)
            .find('.linkedProcesses').on('click', function() {
                var anchor: any = $(this);
                var proc_id: string = anchor.data('proc_id');

                _this.processId = proc_id;
                _this.tabProcessActive = true;

            });
    }
    /***
     * get the selected FSM into FSM var
     */
    getSelectedFsm() {
        this.configParams.getParams().subscribe(() => {
            this.http.get(this.configParams.getDataUrl() + 'api/getFSM?fsm_id=' + this.selectedFsmId)
                .map(function(res: any) {
                    return res.json();
                })
                .subscribe(fsm => {

                    this.FSM = JSON.stringify(fsm, null, 4);
                    this.fsmjson = JSON.parse(this.FSM);
                    var highlightedFSM: string = syntaxHighlight(this.FSM);
//                    this.linkedFSM = highlightedFSM.replace(/((\w+-\w+-\w+-\w+-\w+))/g,
                    this.linkedFSM = highlightedFSM.replace(/(entryProcessor|intentProcessor)":<\/span> <span class="string">"(.*)"<\/span>/g,
                        '$1":</span> "<a data-proc_id="$2" class="linkedProcesses" href="javascript:void(0)"><span class="string">$2</span></a>"');

                    this.showFormatted();
                }
                );
        });
    }

    openProcessor(processor_id:string) {
        alert(processor_id);
    }

    /**
     * add a new FSM
     */
    addNewFsm() {
        // start new mode
        this.addNewMode = true;
        // calculate FsmId
        if (isNaN(this.maxId)) {
            // Last one is 'Add New ...'
            if (this.fsmIds.length >= 2) {
                var tempId = this.fsmIds[this.fsmIds.length - 2];
                if (isNaN(parseInt(tempId))) {
                    // search for separator '_'
                    var arr = tempId.split('_');
                    if (arr.length === 1) {
                        // no '_' found just append 1 with separator at the end
                        tempId = tempId + '_1';
                    } else {
                        var lastNumber = arr[arr.length - 1];
                        if (isNaN(parseInt(lastNumber))) {
                            // last token is not number append 1 with separator at
                            // the end
                            tempId = tempId + '_1';
                        } else {
                            // if '_' found get last numbner increment by 1 &
                            // append incremented number again to create unique
                            // id
                            var incrementedNumber = parseInt(lastNumber, 10) + 1;
                            tempId = tempId.substring(0, tempId.length - lastNumber.length)
                                + incrementedNumber.toString();
                        }
                    }
                    this.selectedFsmId = tempId;
                    this.FSM = '';
                } else {
                    this.selectedFsmId = (parseInt(tempId) + 1).toString();;
                    this.FSM = '';
                }
            } else {
                this.selectedFsmId = '1';
                this.FSM = '';
            }

        } else {
            this.selectedFsmId = (this.maxId + 1).toString();
            this.FSM = '';
        }
        this.fsmjson = this.FSM;
    }

    /**
     * set edit mode
     * @param b
     */
    setEditMode(b: boolean) {
        this.editMode = b;
    }

    deleteFSM() {
        if (!confirm('Are you sure?')) {
            return;
        }

        // now post
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.configParams.getParams().subscribe(() => {
            this.http.post(this.configParams.getDataUrl() + 'api/deleteFSM',
                JSON.stringify({ id: this.selectedFsmId }), {
                    headers: headers
                }).subscribe((resp: any) => {

                    this.message = resp.text();

                    console.log('delete FSM response', resp);

                    this.editMode = this.addNewMode = false;
                    this.selectedFsmId = undefined;
                    this.getAllFsms();

                },
                err => alert('error in deleteFSM. status:' + err.status)
                );
        });
    }

    logsSelected() {
        this.getAllLogRows();
    }
    timestampToString(ts:any) {
        return new Date(parseInt(ts)).toString();
    }
    
     printDate(timeStamp: number) {
        var temp = new Date(timeStamp);
        var dateStr = 
                    this.padStr(1 + temp.getMonth()) + "/" +
                    this.padStr(temp.getDate()) + " " +
                    this.padStr(temp.getHours()) + ":" +
                    this.padStr(temp.getMinutes()) + ":" +
                    this.padStr(temp.getSeconds());

        return dateStr;
      
    }

    padStr(i:number) {
        return (i < 10) ? "0" + i : "" + i;
    }

    stringify(jsons:any) {
        return JSON.stringify(jsons);
    }

    /**
     * check on every change
     *
     */
    fsmChanged() {
        try {
            // parse and check
            JSON.parse(this.FSM);
        } catch (e1) {
            console.log('not a valid JSON. Please confirm at www.jsoneditoronline.org:' + e1);
        }
    }
    /***
     * save the FSM
     */
    saveFsm() {
        var FSM:any;
        try {
            // parse and check
            FSM = JSON.parse(this.FSM);
        } catch (e1) {
            alert('not a valid JSON. Please confirm at www.jsoneditoronline.org:' + e1);
        }

        this.fsmjson = FSM;

        // overwrite the id if anyone had any thoughts
        FSM.id = this.selectedFsmId;
        // now post
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.configParams.getParams().subscribe(() => {
            this.http.post(this.configParams.getDataUrl() + 'api/upsertFSM',
                JSON.stringify(FSM), {
                    headers: headers
                }).subscribe((resp: any) => {

                    this.message = resp.text();

                    console.log('save FSM response', resp);

                    if (this.addNewMode) {
                        // update the dropdown with a new FSM
                        this.fsmIds.pop();
                        this.fsmIds.push(this.selectedFsmId);
                        this.fsmIds.push('Add New...');
                    }

                    this.editMode = this.addNewMode = false;
                    this.getAllFsms();

                },
                err => alert('error in upsertFSM. status:' + err.status)
                );
        });

    }

    /**
     * cancel
     */
    cancelEdit() {
        this.editMode = false;
        // if we cancel out of addNew mode, the selectedFsmId isnt valid
        this.selectedFsmId = (this.addNewMode) ? '1' : this.selectedFsmId;
        this.addNewMode = false;

    }
    /***
     * ctor
     * @param http
     */
    constructor(public configParams: ConfigParams, public http: Http, public elementRef: ElementRef) {

        // get all from db
        this.logPageIndex = 0;
        this.logPageSize = 100;
        this.getAllFsms();
        
        this.gridOptions = <GridOptions>{};
        this.showGrid = true;
        this.rowModelType = 'pagination';
        var that:any = this;

        this.pageSize = 10;
        this.pageIndex = 0;
        this.startRow = 0;
        this.endRow = 10;

        this.FSM = '';

    }

}

