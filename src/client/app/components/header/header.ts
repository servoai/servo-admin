import {Component, ElementRef} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';
import {DROPDOWN_DIRECTIVES, ACCORDION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {ROUTER_DIRECTIVES} from '@angular/router';

@Component({
  selector: 'header-notification',
  templateUrl: '../app/components/header/header-notification.html',
  directives: [DROPDOWN_DIRECTIVES, ROUTER_DIRECTIVES],
  viewProviders: [DROPDOWN_DIRECTIVES]
})
export class HeaderNotification {
  toggled(open:boolean):void {
    console.log('Dropdown is now: ', open);
  }
}

@Component({
  selector: 'sidebar-search',
  templateUrl: '../app/components/header/sidebar-search.html',
  directives: []
})
export class SidebarSearch {

}

@Component({
  selector: 'sidebar',
  templateUrl: '../app/components/header/sidebar.html',
  directives: [ROUTER_DIRECTIVES, SidebarSearch, ACCORDION_DIRECTIVES]
})
export class Sidebar {
}

@Component({
  selector: 'header',
  templateUrl: '../app/components/header/header.html',
  directives: [Sidebar, HeaderNotification]
})
export class Header {

}

@Component({
  selector: 'wrapper',
  template: `<div id="wrapper">
      <header></header>
      <div id="page-wrapper" style="min-height: 561px;">
        <ng-content></ng-content>
      </div>
    </div>`,
  directives: [Header, CORE_DIRECTIVES]
})
export class WrapperCmp {
}
