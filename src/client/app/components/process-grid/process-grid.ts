import {Component, DynamicComponentLoader, ElementRef, ComponentRef, OnInit, ViewContainerRef} from '@angular/core';
import {NgClass, CORE_DIRECTIVES} from '@angular/common';
import {Http} from '@angular/http';
import {AgGridNg2} from 'ag-grid-ng2/main'
import {GridOptions} from 'ag-grid/main';
import {ConfigParams} from '../../services/configparams';
import {ChatBox, UserModel} from '../chat-box/chat-box';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Component({
    selector: 'process-grid',
    templateUrl: '../app/components/process-grid/process-grid.html',
    styleUrls: ['../app/components/process-grid/process-grid.css'],
    directives: [CORE_DIRECTIVES, NgClass, AgGridNg2]
})

export class ProcessGrid implements OnInit{

    dcl: DynamicComponentLoader;
    elRef: ElementRef;
    viewContainerRef: ViewContainerRef;
    columnDefs: any[];
    currentProcesses: Array<ProcessModel>;
    fsms: any[];
    latestProcessesObserver: any;
    
    private gridOptions: GridOptions;
    private showGrid: boolean;
    private rowData: any[];
    private gridColumnDefs: any[];
    private rowCount: string;
    private rowModelType: string;
    private pageSize: number;
    private pageIndex: number;
    private startRow: number;
    private endRow: number;

    /**
    * Event Called on initialization
    */
    ngOnInit() {
        //get all FSMs
        this.getAllFsms();
        //init process observer
        this.runLatestProcessesObserver();
    }
    /**
  * When object is destroyed
      */
    ngOnDestroy() {
        if (this.latestProcessesObserver !== null) {
            //this.latestProcessesObserver.unsubscribe();
        }
    }
    runLatestProcessesObserver() {
        //start observer
        //fetch all FSMs and Process after every 6 seconds
        var obsrvr = Observable.interval(6000);
        this.latestProcessesObserver = obsrvr.subscribe(() => {
            this.getAllFsms();
        });
    }
    /***
     * createColumnDefs()
     * create column for grid
     */
    private createColumnDefs() {
        this.gridColumnDefs = [
            {
                headerName: 'Status', field: 'status', width: 100, 
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' }

            },
            {
                headerName: 'Intent', field: 'intent', width: 100, 
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' }
            },
            {
                headerName: 'State', field: 'state', 
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' }
            },
            {
                headerName: 'Last Speaker', field: 'speaker', 
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' }
            },
            {
                headerName: 'Idle', field: 'idle', 
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' }
            },
            {
                headerName: 'Channel', field: 'channel', width: 100, 
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' }
            },
            {
                headerName: 'User ID', field: 'userId', width: 100, 
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' }
            },
            {
                headerName: 'CSR ID', field: 'CSRId', width: 100, 
                filter: 'text', filterParams: { apply: true, newRowsAction: 'keep' }
            }
        ];
    }

    private calculateRowCount() {
        if (this.gridOptions.api && this.rowData) {
            var model = this.gridOptions.api.getModel();
            var totalRows = this.rowData.length;
            var processedRows = model.getRowCount();
            this.rowCount = processedRows.toLocaleString() + ' / ' + totalRows.toLocaleString();
        }
    }

    private onModelUpdated() {
        console.log('onModelUpdated');
        //this.calculateRowCount();
    }

    private onReady() {
        console.log('onReady');
        //this.calculateRowCount();
    }
    
    private calculateIdleTime(updatedTimestamp:any){
        var msDiff = new Date().getTime() - updatedTimestamp;
        msDiff = msDiff / 1000;
        var inDays = Math.floor(msDiff / 3600 / 24);
        var inHours = Math.floor((msDiff - inDays * 3600 * 24) / 3600);
        var inMin = Math.floor((msDiff - inDays * 3600 * 24 - inHours * 3600) / 60);
        var inSecs = Math.floor(msDiff - inDays * 3600 * 24 - inHours * 3600 - inMin * 60);
        var idleTime = (inDays) + 'd ' + (inHours) + 'h ' + inMin + 'm ' + (inSecs) + 's';
        return idleTime;
    }
    
    /***
     * add processes in datasource
     */
    addProcessesInDatasource(p:any, index:number) {

        console.log('process : ' + p.value.process);
        var processFSMId = p.value.process.fsm_id;
        var processId = p.id;
        var currentStateId = p.value.process.currentStateId;
        var speaker = p.value.process.lastTalked;
        var idleTime = this.calculateIdleTime(p.value.process.updatedTimestamp);
        var customer: UserModel = new UserModel(p.value.process.customer);
        var agent = '';
        if (p.value.process.hasOwnProperty('chatRequired')) {
            agent = 'Agent';
        } else {
            agent = 'chatFlow';
        }
        var channel = p.value.process.customer.hasOwnProperty('fbId') ? 'FB' : 'SMS';

        for (var fsm of this.fsms) {
            var fsmId = fsm.id;
            var states: Array<string> = new Array<string>();

            for (var state of fsm.states) {
                states.push(state.name);
            }

            if (fsmId === processFSMId) {
                var stateName = fsm.states[currentStateId] ? fsm.states[currentStateId].name : "";
                var process = new ProcessModel(
                    agent,
                    p.value.process.lastIntent,
                    '',
                    stateName,
                    speaker,
                    '',
                    idleTime,
                    customer,
                    processId,
                    fsmId,
                    fsm.name,
                    channel,
                    states
                );
                
                process.updatedTimestamp = p.value.process.updatedTimestamp;

                delete p.value.process.messages;
                process.processJSON = JSON.stringify(p.value.process, null, 4);
                this.currentProcesses.push(process);
                
                
                this.rowData.push({status:agent,
                                   intent:p.value.process.lastIntent,
                                   state:stateName,
                                   speaker:speaker,
                                   idle:idleTime,
                                   channel:channel,
                                   userId:customer.getFullName(),
                                   CSRId:'rep1'});
                break;
            }
        }
    }
    
    private getGridRows(params:any){
        // this code should contact the server for rows. however for the purposes of the demo,
            // the data is generated locally, a timer is used to give the experience of
            // an asynchronous call
            console.log('asking for ' + params.startRow + ' to ' + params.endRow);
            this.startRow = params.startRow;
            this.endRow = params.endRow;
            //setTimeout( function() {
                // take a chunk of the array, matching the start and finish times
                    var rowsThisPage = this.rowData.slice(params.startRow, params.endRow);
                // see if we have come to the last page. if we have, set lastRow to
                // the very last row of the last page. if you are getting data from
                // a server, lastRow could be returned separately if the lastRow
                // is not in the current page.
                    var lastRow = -1;
                    if (this.rowData.length <= params.endRow) {
                        lastRow = this.rowData.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
              //  }, 500);
    }
    
    private createNewDatasource() {
        if (!this.rowData) {
            // in case user selected 'onPageSizeChanged()' before the json was loaded
            return;
        }
        var dataSource = {
            //rowCount: ???, - not setting the row count, infinite paging will be used
            pageSize: this.pageSize, // changing to number, as scope keeps it as a string
            getRows: (params:any) => {
                this.getGridRows(params);
            }
        };

        this.gridOptions.api.setDatasource(dataSource);
    }
    /***
     * get all process from the server
     */
    getAllProcesses() {

        this.configParams.getParams().subscribe(() => {
            this.http.get(this.configParams.getDataUrl()
                + 'apiprocess/getLatestProcesses' +
                '?pageIndex=0' +
                '&pageSize=50')
                .map(function(res: any) {
                    return res.json();
                })
                // Subscribe to the observable to get the parsed  object and attach it to the
                .subscribe(processes => {

                    // the key is the updatedTimestamp
                    // id is the process unique id
                    this.currentProcesses = [];
                    this.rowData = [];
                    var i = 0;
                    for (var p of processes) {
                        this.addProcessesInDatasource(p, i++);
                    }
                    
                    if (this.startRow === 0) {
                        this.createNewDatasource();
                    }
                    else
                    {
                        var updatedNodes: any;
                        updatedNodes = [];
                        var localRef = this;

                        this.gridOptions.api.forEachNode( function(node) {
                            if(node.id >= localRef.startRow && node.id <= localRef.endRow)
                            {
                                var process = localRef.currentProcesses[node.id];
                                var data = node.data;
                                var idleTime = localRef.calculateIdleTime(process.updatedTimestamp);
                                data.idle = idleTime;
                                updatedNodes.push(node);
                            } 
                        });
                        // now tell the grid it needs refresh all these rows
                        this.gridOptions.api.refreshRows(updatedNodes);

                    }
                    
                    
                });
        });

    }
    /***
     * get all the fsms from the server
     */
    getAllFsms() {
        this.configParams.getParams().subscribe(() => {
            this.http.get(this.configParams.getDataUrl()
                + 'api/getAllFSMs') // Call map on the response observable to get the parsed  object
                .map(function(res: any) {
                    return res.json();
                })
                // Subscribe to the observable to get the parsed  object and attach it to the
                .subscribe(fsms => {
                    this.fsms = fsms;
                    this.getAllProcesses();
                }
                );
        });

    }
    /***
     * start a process
     * @param e
     */
    private onRowClicked(event:any) {
        var rowIndex = event.node.childIndex;
        var process = this.currentProcesses[rowIndex];
        if (process != null) {
            if (ChatBox.processIds.indexOf(process.processId) !== -1) {
                alert('Chat for this Process is already open.');
                return;
            }


            this.dcl.loadNextToLocation(
                ChatBox,
                this.viewContainerRef
            ).then((compRef: ComponentRef<ChatBox>) => {
                compRef.instance.processInfo = process;
                compRef.instance.init(compRef);
            });
        }
    }
    /**
     * ctor
     * @param http
     */
    constructor(dcl: DynamicComponentLoader, elementRef: ElementRef, 
        public configParams: ConfigParams, public http: Http, vcr: ViewContainerRef) {
        this.getAllFsms();
        this.gridOptions = <GridOptions>{
        getRowStyle: (params:any) => {
                var process = this.currentProcesses[params.node.id];
                if (process.agent === 'Agent' ) {
                    return {'background-color': '#df8d8b'}
                }
                else
                {
                    return {'background-color': '#accb9e'}
                }
            }
        };
        this.showGrid = true;
        this.rowModelType = 'pagination';
        this.createColumnDefs();
        this.pageSize = 20;
        this.pageIndex = 0;
        this.startRow = 0;
        this.endRow = 20;
        //this.getAllProcesses();
        this.dcl = dcl;
        this.elRef = elementRef;
        this.viewContainerRef = vcr;
    }
}

export class ProcessModel {
    agent: string;
    intent: string;
    priority: string;
    state: string;
    states: Array<string>;
    speaker: string;
    status: string;
    channel: string;
    idle: string;
    user: UserModel;
    orderedFields: any[];
    rowClass: string;
    processId: string;
    fsmId: string;
    fsmName: string;
    processJSON: string;
    updatedTimestamp: string;

    constructor(agent: string, intent: string, priority: string, state: string,
        speaker: string, status: string, idle: string, user: UserModel,
        processId: string, fsm_id: string, fsm_name: string, channel: string,
        states: Array<string>) {

        this.agent = agent;
        this.intent = intent;
        this.priority = priority;
        this.state = state;
        this.states = states;
        this.speaker = speaker;
        this.status = status;
        this.idle = idle;
        this.user = user;
        this.channel = channel;

        this.processId = processId;
        this.fsmId = fsm_id;
        this.fsmName = fsm_name;

        this.orderedFields = [this.agent, this.intent, this.state,
            this.speaker, this.idle, this.channel, this.user.getFullName(), 'rep1'];
        if (this.agent === 'Agent') {
            this.rowClass = 'row-doubt';
        } else {
            this.rowClass = 'row-high';
        }
        //    else if (this.confidence === 'Med') {
        //            this.rowClass = 'row-medium';
        //        }
    }
}
