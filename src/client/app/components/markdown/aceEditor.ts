import {ElementRef, Directive, EventEmitter} from '@angular/core';
//import {Directive, EventEmitter} from '@angular/core';


//declare the ace library as Magic;
declare var ace:any;

// Note the aceEditor doesn't have a template so is a Directive
@Directive({
    selector: '[ace-editor]',
    inputs: [
        'markdown',
        'editMode'
    ],
    outputs: ['contentChange: change']
})
export class AceEditor {
    // http://ace.c9.io/#nav=api&api=editor
    editor:any;

    /** When the markdown content changes we broadcast the entire document. */
    contentChange: EventEmitter<any> = new EventEmitter();

    constructor(elementRef: ElementRef) {
        // Note the constructor doesn't have access to any data from properties
        // We can instead use a setter

        // This is the <div ace-editor> root element
        // Ideally this wouldn't be required
        var el = elementRef.nativeElement;

        el.classList.add('editor');
        el.style.height = '450px';
//        el.style.width = "400px";

        this.editor = ace.edit(el);
        this.editor.setReadOnly(true);
        this.editor.setTheme("ace/theme/monokai");
        this.editor.setShowFoldWidgets(true);
        this.editor.getSession().setMode('ace/mode/javascript');
        this.editor.getSession().setNewLineMode('unix');

        //this.editor.$blockScrolling = Infinity;

        this.editor.on('change', (e:any) => {
            // Discard the delta (e), and provide whole document
            this.contentChange.next(this.editor.getValue());
        });

    }

    set markdown(text:string){
        this.editor.setValue(text);
        this.editor.clearSelection();
        this.editor.focus();
    }
    
    set editMode(mode: boolean){
        this.editor.setReadOnly(!mode);
    }


}
