import {Component} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';
import {WrapperCmp} from '../header/header';

@Component({
  selector: 'notifications',
  templateUrl: '../app/components/notifications/notifications.html',
  styleUrls: ['../app/components/notifications/notifications.css'],
  directives: [WrapperCmp, CORE_DIRECTIVES]
})
export class NotificationsPage {
}
