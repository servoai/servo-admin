import {Component, ViewEncapsulation, ComponentRef, ComponentFactory, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CORE_DIRECTIVES} from '@angular/common';
import {ConfigParams} from '../../services/configparams';
import {Http, Headers} from '@angular/http';
import {ProcessModel} from '../process-grid/process-grid';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/interval';
//import {TimeAgoPipe} from 'angular2-moment/TimeAgoPipe';


@Component({
    selector: 'chat-box',
    //    pipes: [TimeAgoPipe],
    styleUrls: ['../app/components/chat-box/chat-box.css'],
    templateUrl: '../app/components/chat-box/chat-box.html',
    directives: [CORE_DIRECTIVES],
    encapsulation: ViewEncapsulation.None
})

export class ChatBox implements OnInit {
    static boxCount: number = 0;
    static processIds: Array<string> = [];
    static CHATBOX_WIDTH: number = 300;
    sessionId: string;             //Chat session id
    processInfo: ProcessModel;
    isBot: boolean;
    cmpRef: ComponentRef<ChatBox>;
    messages: Array<MessageModel>;
    messageBodyActOnProcess: string;
    isObsrvrRunning: boolean;
    latestMsgObsrvr: any;
    latestMsgTS: number;
    messageText: string;
    isSendingMessage: boolean;
    isGettingMessages: boolean;
    toUser: UserModel;
    isExpanded: boolean;
    actDone: boolean;
    stateSelected: string;
    stateBodyText: string;

    constructor(public configParams: ConfigParams, public http: Http, private router: Router) {
        this.isBot = true;
        this.messages = [];
        this.messageText = '';
        this.stateSelected = '';
        this.stateBodyText = '';
        this.isObsrvrRunning = false;
        this.isSendingMessage = this.isGettingMessages = false;
        this.isExpanded = false;

        var _this = this;

        //whenever moves to a new route
        this.router.changes.subscribe(() => {
            try {
                _this.close();
            } catch (e) { }
        });
    }

    /**
     * Initializes Chat
     */
    init(cmpRef: ComponentRef<ChatBox>) {
        this.cmpRef = cmpRef;
        var el = this.cmpRef.location.nativeElement;

        var $domEl: any = $(el);

        $domEl.css('left', ChatBox.boxCount * ChatBox.CHATBOX_WIDTH);
    }

    getLatestMessages(laterThan: number = 0) {
        // do not try to get messages while sending - this leads to confusion
        if (this.isSendingMessage && !this.isGettingMessages) {
            return;
        }
        this.isGettingMessages = true;
        this.configParams.getParams().subscribe(() => {
            this.http.get(this.configParams.getDataUrl()
                + 'apiprocess/getLatestMessages' +
                '?processId=' + this.processInfo.processId +
                '&laterThan=' + laterThan)
                .map(function(res: any) {
                    return res.json();
                }).subscribe(messages => {
                    this.isGettingMessages = false;
                    for (var msg of messages) {

                        // get the process info from the chatbox
                        msg.processInfo = this.processInfo;
                        var msg_model = new MessageModel(msg);
                        // we have to check again since  after CSR added new message, we still have pending queries with old laterThan
                        if (this.messages.length === 0 || this.messages[this.messages.length - 1].timestamp < msg_model.timestamp) {
                            this.messages.push(msg_model);

                            if (typeof this.toUser === 'undefined') {
                                this.toUser = msg_model.toUser;
                            }

                            this.latestMsgTS = msg_model.timestamp;
                        }

                    }

                    if (this.isObsrvrRunning === false) {
                        this.runLatestMessagesObserver();
                        this.isObsrvrRunning = true;


                        setTimeout(() => {
                            this.scrollToLatestMessage();
                        }, 300);
                    }
                });
        });
    }

    scrollToLatestMessage() {
        var $domEl: any = $(this.cmpRef.location.nativeElement);
        var $messages_container: any = $domEl.find('.messages_container');

        var lastMsgOffset: any = $messages_container.find('.chat li:last-child').offset();

        if (typeof lastMsgOffset === 'undefined') {
            return;
        }

        $messages_container.scrollTop(lastMsgOffset.top);
    }

    runLatestMessagesObserver() {
        if (this.isObsrvrRunning === true) {
            return;
        }

        //start observer
        //fetch messages after every sec
        var obsrvr = Observable.interval(1000);
        this.latestMsgObsrvr = obsrvr.subscribe(() => {
            this.getLatestMessages(this.latestMsgTS);
        });
    }

    actOnProcess() {
        this.actDone = true;
        var body = {
            processId: this.processInfo.processId,
            stateName: this.stateSelected,
            messageBody: this.stateBodyText,
            userId: 'AA-rep'
        };
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var bodys = JSON.stringify(body);
        this.configParams.getParams().subscribe(() => {
            this.http.post(this.configParams.getDataUrl() + 'api/actOnProcess', bodys, {
                headers: headers
            }).map(function(res: any) {
                return { status: res.status, message: res.json() };
            })
                .subscribe(
                (data: any) => console.log('act sent', data),
                (err: any) => console.error('act err', err),
                () => { this.actDone = true; console.log('act Done, complete'); }
                );
        });
    }
    /**
     * Expands the ChatBox
     */
    protected expand() {
        this.isExpanded = !this.isExpanded;
        var el = this.cmpRef.location.nativeElement;
        var $domEl: any = $(el);

        if (this.isExpanded) {

            var localProcessJSON = _.clone(this.processInfo.processJSON);
            var localProcessJSONString = JSON.parse(localProcessJSON);
            localProcessJSONString.fsm = undefined;
            var highlightedHtml: any = syntaxHighlight(JSON.stringify(localProcessJSONString));

            setTimeout(() => {
                $domEl.find('.processJSON').html('<pre>' + highlightedHtml + '</pre>');
                //                $domEl.find('.state-box').css('left', (ChatBox.boxCount * ChatBox.CHATBOX_WIDTH + 65));
            }, 200);

            $domEl.css('width', 2.5 * ChatBox.CHATBOX_WIDTH);
        } else {
            $domEl.css('width', ChatBox.CHATBOX_WIDTH);
        }
    }

    protected close() {
        if (this.isExpanded) {
            this.expand();
        }
        //   if (confirm('Are you sure?')) {
        //            console.log(this.ref.nativeElement);
        this.cmpRef.destroy();
        //  }
    }

    /**
     * Sends Message to end-user.
     */
    sendMessage() {

        var text = this.messageText.trim();

        if (text.length === 0) {
            alert('Please enter some text to send.');
            return;
        }
        this.isSendingMessage = true;

        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        // here we make an example
        var jsonMessage = {
            // here the either the customer service representative (the company). if AA then the phone number should be the origin
            // take that from a global "CSR" entity you maintain on the client
            fromUser: {
                //userId:'USER0', not used
                firstName: 'AA',
                lastName: 'rep1',
                phoneNumber: '+17079294514' // the origin number in our first SMS demo
            },
            // take that from the process entity (every chat window should have a reference to the process entity to which it belongs)
            toUser: {
                firstName: this.toUser.firstName,
                lastName: this.toUser.lastName,
                phoneNumber: this.toUser.phoneNumber, // either this, or:
                fbId: this.toUser.fbId
            },
            // that this from the chat window
            text: this.messageText,
            // take these from the process entity
            fsm_id: this.processInfo.fsmId,
            processId: this.processInfo.processId
        };
        var body = JSON.stringify(jsonMessage);
        this.configParams.getParams().subscribe(() => {
            this.http.post(this.configParams.getDataUrl() + 'apiprocess/sendMessage', body, {
                headers: headers
            }).map(function(res: any) {
                return { status: res.status, message: res.json() };
            })
                .subscribe(
                (data: any) => this.onMessageSent(data),
                (err: any) => this.onMessageError(err),
                () => console.log('send message complete')
                );
        });
    }

    /**
     * Takeover Bot conversation
     */
    takeControl() {
        this.isBot = false;
        this.messageText = '';

        var $domEl: any = $(this.cmpRef.location.nativeElement);
        var shiftDown = false;
        var _this = this;

        setTimeout(() => {
            $domEl.find('.message-box').unbind('keydown').on('keydown', function(e: any) {
                // shift key code 16
                if (e.which === 16) {
                    shiftDown = true;
                }
                // if enter pressed and shift is pressed then we send message
                if (e.which === 13 && shiftDown) {
                    _this.sendMessage();
                    e.preventDefault();
                }
            }).unbind('keyup').on('keyup', function(e: any) {
                if (e.which === 16) {
                    shiftDown = false;
                }
            });
        }, 200);
    }

    /**
     * HandOff and give controll back to bot
     */
    handOff() {
        this.isBot = true;
    }

    submitStateInfo() {
        this.actOnProcess();
    }


    /************** Events ******************/

    onMessageSent(data: any) {
        this.isSendingMessage = false;
        console.log('message sent:', this.messageText);
        this.messageText = '';

        //        setTimeout(() => {
        //            this.scrollToLatestMessage();
        //        }, 300);

    }

    onMessageError(err: any) {
        this.isSendingMessage = false;
        alert('Problem sending message, please try again or contact support.');
    }

    /**
     * Event Called on initialization
     */
    ngOnInit() {
        //get all messages till now
        this.getLatestMessages();
        ChatBox.processIds.push(this.processInfo.processId);
        ChatBox.boxCount++;
    }
    /**
     * When object is destroyed
     */
    ngOnDestroy() {
        if (this.latestMsgObsrvr) {
            this.latestMsgObsrvr.unsubscribe();
        }

        ChatBox.processIds.splice(ChatBox.processIds.indexOf(this.processInfo.processId), 1);
        ChatBox.boxCount--;
    }

    onChatBoxClick(e: MouseEvent) {
        var el = this.cmpRef.location.nativeElement;
        var domEl: any = $(el);
        domEl.siblings('chat-box').css('z-index', 999);
        domEl.css('z-index', 1000);
    }
}

/**
 * return HTML with syntax highlighting
 * @param json
 * @returns {string}
 */
function syntaxHighlight(json: any) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
        function(match: any) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
}

export class UserModel {
    firstName: string;
    lastName: string;
    phoneNumber: string;

    fbId: string;

    constructor(user_obj: any) {
        this.firstName = '';
        this.lastName = '';
        this.phoneNumber = user_obj.phoneNumber;
        this.fbId = user_obj.fbId;

        if (user_obj.hasOwnProperty('firstName')) {
            this.firstName = user_obj.firstName;
        }
        if (user_obj.hasOwnProperty('lastName')) {
            this.lastName = user_obj.lastName;
        }
    }

    getFullName() {
        var fullName = this.firstName + (this.lastName.length !== 0 ? (' ' + this.lastName) : '');

        if (fullName.length === 0) {
            fullName = 'No Name';
        }

        return fullName;
    }
}

class MessageModel {
    fsmId: number;
    processId: string;
    text: string;
    timestamp: number;
    fromUser: UserModel;
    toUser: UserModel;
    fromUserType: string;
    dateTime: Date;
    isCustomer: boolean;
    processInfo: ProcessModel;

    constructor(msg_obj: any) {
        this.fsmId = msg_obj.fsm_id;
        this.processId = msg_obj.processId;
        this.text = msg_obj.text;
        this.fromUser = new UserModel(msg_obj.fromUser);
        this.toUser = new UserModel(msg_obj.toUser);
        this.timestamp = msg_obj.timestamp;
        this.dateTime = new Date(this.timestamp);
        this.isCustomer = false;
        this.processInfo = msg_obj.processInfo;

        // if the customer name of the process == the fromUser of the message
        //        if (msg_obj.processInfo.customer.firstName===msg_obj.fromUser.firstName &&
        //            msg_obj.processInfo.customer.lastName===msg_obj.fromUser.lastName) {
        //            this.isCustomer = true;
        //        }
        //        if (msg_obj.hasOwnProperty('userType')) {
        //            if (msg_obj.fromUser.firstName===)
        //            this.fromUserType = msg_obj.userType.toString().toLowerCase();
        //        } else {
        //            var userTypes = ['customer', 'servo', 'csrc'];
        //            var index = Math.floor((Math.random() * 10)) % 3;
        //            var userType = userTypes[index];
        //
        //            this.fromUserType = userType;
        //        }
        //

        if (msg_obj.hasOwnProperty('fromUserType')) {
            this.fromUserType = msg_obj.fromUserType.toLowerCase();
        }
        if (this.fromUserType === 'customer') {
            this.isCustomer = true;
        }

        if (!this.fromUserType || this.fromUserType === '') {
            this.fromUserType = 'csrc';
        }
    }

}
