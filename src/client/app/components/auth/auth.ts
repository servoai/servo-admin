import {Component} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';

@Component({
  selector: 'auth',
  templateUrl: '../app/components/auth/auth.html',
  styleUrls: ['../app/components/auth/auth.css'],
  directives: [CORE_DIRECTIVES]
})
export class AuthPage {
}
