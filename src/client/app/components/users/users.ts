import {Component} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common'
import {WrapperCmp} from '../header/header';
import {ConfigParams} from '../../services/configparams';
import {Http, Headers} from '@angular/http';
import {ProcessGrid} from '../process-grid/process-grid';

@Component({
    //moduleId: module.id,
    selector: 'users',
    templateUrl: '../app/components/users/users.html',
    styleUrls: ['../app/components/users/users.css'],
    directives: [WrapperCmp, CORE_DIRECTIVES, ProcessGrid]
})
export class UsersPage {
    startProcess: boolean;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    fsmMessage: string;
    selectedFsmId: string;
    fsmIds: Array<string>;
    fbIds: Array<any>;
    selectedFbId: number;
    selectedFbName: string;

    fsmStarted(data:any) {
        if (data.status===400) {
            this.fsmMessage = 'Conversation error: ' + data.message.message;
            this.fsmMessage += "\nPlease see chatFlows|Logs for more details";
            console.error(this.fsmMessage);
        } else {
            console.log('fsm Started',data);
            this.fsmMessage = 'Conversation started';
        }

    }

    fsmError(err: any) {
        console.error('error in startfsm:', err);
        this.fsmMessage = 'Conversation error:' + (err.message);
        this.fsmMessage += "\nPlease see chatFlows|Logs for more details";

    }

    /***
     * get all the fsms from the server
     */
    getAllFsms() {
        this.configParams.getParams().subscribe(()=> {
            this.http.get(this.configParams.getDataUrl()
                            + 'api/getAllFSMs') // Call map on the response observable to get the parsed  object
                .map(function (res:any) {
                    return res.json();
                })
                // Subscribe to the observable to get the parsed  object and attach it to the
                .subscribe(fsms => {

                    // the server converts to an array
                    this.fsmIds = fsms.map((obj:any) => {

                        return obj.id;
                    });
                    console.log('conversations get all fsms', fsms, this.fsmIds);
                    // simulate the $event sent on a click
                    this.onFsmIdSelected({currentTarget: {value: this.selectedFsmId || this.fsmIds[0].toString()}});
                }
            );
        });

    }

    /***
     * select handler
     * @param ev
     */
    onFsmIdSelected(ev:any) {
        console.log('conversations onFsmIdSelected', ev.currentTarget.value);

        this.selectedFsmId = ev.currentTarget.value;

    }

    onFbIdSelected(ev:any) {
        console.log('conversations onFbIdSelected', ev.currentTarget.value);

        this.selectedFbId = this.fbIds[ev.currentTarget.selectedIndex].id;
        this.selectedFbName = this.fbIds[ev.currentTarget.selectedIndex].name;

    }

    onFbStartClick(e:any) {

        // do not double click
        if (this.fsmMessage==='Sending...')
            return;
        // post startFSM api
        this.fsmMessage = 'Sending...';
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        var postId = Math.random().toString();//to prevent multiple posts
        var body =  JSON.stringify({'fsm_id':this.selectedFsmId ,
            'users':[
                {'fbId':this.selectedFbId,
                 'firstName':this.selectedFbName}
            ],
            'postId':postId
        });
        this.configParams.getParams().subscribe(()=> {
            this.http.post(this.configParams.getDataUrl() + 'api/startFSM', body, {
                headers: headers
            }).map(function (res:any) {
                return {status: res.status, message: res.json()};
            })
                .subscribe(
                (data:any) => this.fsmStarted(data),
                (err:any) => this.fsmError(err),
                () => console.log('start fsm Complete')
            );
        });

    }
    /***
     * start a process
     * @param e
     */
    onstartclick(e:any) {

        // do not double click
        if (this.fsmMessage==='Sending...')
            return;
        // post startFSM api
        this.fsmMessage = 'Sending...';
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        var postId = Math.random().toString();//to prevent multiple posts
        var body =  JSON.stringify({'fsm_id':this.selectedFsmId ,
                                    'users':[
                                        {'phoneNumber':this.phoneNumber,
                                            'firstName':this.firstName,
                                            'lastName':this.lastName}
                                    ],
                                    'postId':postId
                                });
        this.configParams.getParams().subscribe(()=> {
            this.http.post(this.configParams.getDataUrl() + 'api/startFSM', body, {
                headers: headers
            }).map(function (res:any) {
                return {status: res.status, message: res.json()};
            })
                .subscribe(
                (data:any) => this.fsmStarted(data),
                (err:any) => this.fsmError(err),
                () => console.log('start fsm Complete')
            );
        });

    }

    onGetLatestProcesses() {
        this.configParams.getParams().subscribe(()=> {
            this.http.get(this.configParams.getDataUrl()
                + 'apiprocess/getLatestProcesses' +
                '?pageIndex=0' +
                '&pageSize=50')
                .map(function (res:any) {
                    return res.json();
                })
                // Subscribe to the observable to get the parsed  object and attach it to the
                .subscribe(processes => {

                    // the key is the updatedTimestamp
                    // id is the process unique id
                    console.info('processes:', processes);
                });
        });
    }


    messageSent(data:any) {
        console.log('message sent. update chat window?');
    }


    messageError(err:any) {
        console.log('message error. update chat window with error?',err);
    }

    onGetMessages() {
        var thisProcessId = 'dadbda1b-48f3-4824-a5ed-272bc9a03cb9';
        this.configParams.getParams().subscribe(()=> {
            this.http.get(this.configParams.getDataUrl()
                + 'apiprocess/getLatestMessages' +
                '?processId=' + thisProcessId +
                '&laterThan=1460492730645')
                .map(function (res:any) {
                    return res.json();
                })
                .subscribe(messages => {

                    console.info('messages:', messages);
                });
        });
    }
//    onSendMessage()  {
//
//        var headers = new Headers();
//        headers.append('Content-Type', 'application/json');
//
//        // here we make an example
//        var jsonMessage = {
//            // here the either the customer service representative (the company). if AA then the phone number should be the origin
//            // take that from a global 'CSR' entity you maintain on the client
//            fromUser: {
//                //userId:'USER0', not used
//                firstName: 'AA',
//                lastName: 'rep1',
//                phoneNumber:'+1707...' // the origin number in our first SMS demo
//            },
//            // take that from the process entity (every chat window should have a reference to the process entity to which it belongs)
//            toUser: {
//                // userId:'USER156', not used
//                firstName: 'John',
//                lastName: 'Smith',
//                phoneNumber:'+16463730044'
//            },
//            // that this from the chat window
//            text: 'this is me, John. and I\'m gonna type a rather long message to see we are good ' +
//                'with all the things we are at the factory saying as part of a long sales process',
//            // take these from the process entity
//            fsm_id:'2',
//            processId: 'dadbda1b-48f3-4824-a5ed-272bc9a03cb9'
//        };
//        var body = JSON.stringify(jsonMessage);
//        this.configParams.getParams().subscribe(()=> {
//            this.http.post(this.configParams.getDataUrl() + 'apiprocess/sendMessage', body, {
//                headers: headers
//            }).map(function (res:any) {
//                return {status: res.status, message: res.json()};
//            })
//                .subscribe(
//                (data:any) => this.messageSent(data),
//                (err:any) => this.messageError(err),
//                () => console.log('send message complete')
//            );
//        });
//    }

    /**
     * ctor
     * @param http
     */
     constructor(
	    public configParams:ConfigParams,public http:Http) {
            this.startProcess = false;
            this.getAllFsms();
            this.fbIds = [{name: 'Please select',id:undefined},
                    {name: 'Nadav Gur',id:1120548107966263},
                //{name:'Anat Maoz',id: 1177487435594672 },
                {name:'Lee Messinger',id: 972783849505847 }];
            this.selectedFbId = undefined;
            //this.selectedFbName = 'Nadav Gur';
      }

}
