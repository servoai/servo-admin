import {Component} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';
import {WrapperCmp} from '../header/header';

@Component({
    selector: 'documentation',
    templateUrl: '../app/components/documentation/documentation.html',
    styleUrls: ['../app/components/documentation/documentation.css'],
    directives: [WrapperCmp, CORE_DIRECTIVES]
})
export class DocumentationPage {
}
