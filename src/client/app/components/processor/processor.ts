import {Component, Input, EventEmitter} from '@angular/core';
import {CORE_DIRECTIVES, NgIf} from '@angular/common';
import {DbEntity} from '../db-entity/db-entity';
import {Http} from '@angular/http';
import {ConfigParams} from '../../services/configparams';

@Component({
    selector: 'processor',
    templateUrl: '../app/components/processor/processor.html',
    directives: [DbEntity, CORE_DIRECTIVES, NgIf],
    outputs: ['processSelected: proess_selected']
})
export class Processor {
    @Input()
    public set processId(process_id: string) {
        if (typeof process_id === 'undefined' || process_id.length === 0) {
            return;
        }

        this.dbEntityId = process_id;
    }
    public processSelected: EventEmitter<string> = new EventEmitter<string>();

    dbEntityId: string;

    dbEntitySelected(db_entity: string) {
        this.processSelected.next(db_entity);
    }

    loadProcessors() {
        this.configParams.getParams().subscribe(() => {
            this.http.get(this.configParams.getDataUrl() + 'api/loadProcessors').subscribe(function(success) {
                console.log('processors loaded ', success);
            });
        });
    }
    /**
 * ctor
 * @param elementRef
 */
    constructor(public http: Http, public configParams: ConfigParams) {
        console.log('ctor processor');
    }
};

