import {Component} from '@angular/core';
import {CORE_DIRECTIVES} from "@angular/common";
import {WrapperCmp} from '../header/header';

@Component({
  selector: 'buttons',
  templateUrl: '../app/components/buttons/buttons.html',
  styleUrls: ['../app/components/buttons/buttons.css'],
  directives: [WrapperCmp, CORE_DIRECTIVES]
})
export class ButtonsPage {
}
