﻿/// <reference path="jquery.js" />

$(function () {
    Form.init();
    NewsList.init();
})

var NewsList = {
    items: null,
    model: {
        "title": "",
        "link":"",
        "description":"",
        "image":""
    },
    init: function() {
        $.get("newsletter.json",function(json) {
            NewsList.items = json;
            NewsList.refresh();
        })
    },
    add: function() {
        var item = Object.assign({},NewsList.item);
        NewsList.unshift(item);
        NewsList.refresh();
    },
    remove: function(idx) {
        NewsList.items.splice(idx,1);
        NewsList.refresh();
    },
    refresh: function() {
        $(".container").empty();
        var template = $(".template").html();
        var idx = NewsList.items.length;
        for (i=0;i<NewsList.items.length;i++) {
            var item = NewsList.items[i];
            var itemHtml = template;
            itemHtml.replace("##id##",idx);
            itemHtml.replace("##title##",item.title);
            itemHtml.replace("##link##",item.link);
            itemHtml.replace("##description##",item.description);
            itemHtml.replace("##image##",item.image);
            idx--;
            $(".container").prepend(itemHtml);
        }
    },
}

var Form = {
    submit:false,
    init: function(){
        $("input[type=submit]").click(Form.submitForm);
        $("input.remove").click(function () {
            if( confirm("אתה בטוח שאתה מעוניין למחוק את הרשומה"))
                $(this).parent().remove();
        })
        $("input#addNew").click(function () {
        if(!Form.submit)
          Form.addNew();
        });

    },
    submitForm: function (event) {
        Form.submit = true;
        event.preventDefault();
       
        $.ajax({
            type: "POST",
            url: "keep.php",
            data: "json=" + Form.convertToJson(), // serializes the form's elements.
            success: function (data) {
                alert("עודכן בהצלחה"); // show response from the php script.
                Form.submit = false;
            }
        });

    },
    convertToJson: function () {
        var length = $("form > div").length - 1;
        var newJson = '[\n';
        for (var index = 0; index < length; index++) {
            newJson += '{';
            newJson += '"title": "' + $("input[name=title]").eq(index).val() + '",';
            newJson += '"link": "' + $("input[name=link]").eq(index).val() + '",';
            newJson += '"description": "' + $("input[name=description]").eq(index).val() + '",';
            newJson += '"image": "' + $("input[name=image]").eq(index).val() + '"';
            newJson += '}\n';
            if((index + 1) < length)
                newJson += ',';
        }
        newJson += "]";
        return newJson;
    },
    addNew: function () {
        $("form input#addNew").before('<div><input value="" name="title"><input value="" name="link"><input value="" name="description"><input value="" name="image"><input type="button" class="remove" value="הסר"><br></div>')
        Form.init();
    }
}

$.fn.serializeObject = function(){

    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push":     /^$/,
            "fixed":    /^\d+$/,
            "named":    /^[a-zA-Z0-9_]+$/
        };


    this.build = function(base, key, value){
        base[key] = value;
        return base;
    };

    this.push_counter = function(key){
        if(push_counters[key] === undefined){
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(this).serializeArray(), function(){

        // skip invalid keys
        if(!patterns.validate.test(this.name)){
            return;
        }

        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

        while((k = keys.pop()) !== undefined){

            // adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

            // push
            if(k.match(patterns.push)){
                merge = self.build([], self.push_counter(reverse_key), merge);
            }

            // fixed
            else if(k.match(patterns.fixed)){
                merge = self.build([], k, merge);
            }

            // named
            else if(k.match(patterns.named)){
                merge = self.build({}, k, merge);
            }
        }

        json = $.extend(true, json, merge);
    });

    return json;
};