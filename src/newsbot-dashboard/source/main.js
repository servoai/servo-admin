﻿/// <reference path="jquery.js" />

jQuery(document).ready(function () {
   Form.init();
})

var Form = {
    submit:false,
    init: function(){
        $("input[type=submit]").click(Form.submitForm);
        $("input#addNew").click(function () {
            if(!Form.submit)
              Form.addNew();
        });
        Form.initPreview()
        Form.initRemove();
    },
    initPreview: function(){
        $("input.preview").unbind('click').click(function () {
            var title = $(this).parent().parent().find("input[name=title]").val();
            var description = $(this).parent().parent().find("input[name=description]").val();
            var imageUrl = $(this).parent().parent().find("input[name=image]").val();
            var link = $(this).parent().parent().find("input[name=link]").val();


            $(".previewBox").show();
            $(".previewBox a").attr("href", link);
            $(".previewBox img").attr("src", imageUrl);
            $(".previewBox #title").html(title);
            $(".previewBox #description").html(description);

        })
    },
    initRemove: function() {
        $("input.remove").unbind('click').click(function () {
            if( confirm("אתה בטוח שאתה מעוניין למחוק את הרשומה"))
                $(this).parent().parent().remove();
        })
    },
    submitForm: function (event) {
        Form.submit = true;
        event.preventDefault();
        try {
            var json = Form.convertToJson();
            var jsonStr = JSON.stringify(json);
        } catch (err) {
            alert("There was an error with parsing the json. please refresh and try again");
            return;
        }
        $.ajax({
            type: "POST",
            url: "keep.php",
            data: jsonStr, // serializes the form's elements.
            success: function (data) {
                alert("עודכן בהצלחה"); // show response from the php script.
                Form.submit = false;
            }
        });

    },
    convertToJson: function () {
        var length = $("form table tr").length - 1;
        var json = [];
        //var newJson = '[\n ';
        for (var index = 0; index < length; index++) {
            json.push({
                title: $("input[name=title]").eq(index).val(),
                link: $("input[name=link]").eq(index).val(),
                description: $("input[name=description]").eq(index).val(),
                image: $("input[name=image]").eq(index).val()
            });
        }
        return json;
/*
            newJson += '{';
            newJson += '"title": "' + $("input[name=title]").eq(index).val().replace(/"/g, "“") + '",';
            newJson += '"link": "' + $("input[name=link]").eq(index).val().replace(/"/g, "“") + '",';
            newJson += '"description": "' + $("input[name=description]").eq(index).val().replace(/"/g, "“") + '",';
            newJson += '"image": "' + $("input[name=image]").eq(index).val().replace(/"/g, "“") + '"';
            newJson += '}\n';
            if((index + 1) < length)
                newJson += ',';
        }
        newJson += "]";
        return newJson;
*/
        
    },
    addNew: function () {
        $("form table tr").eq(1).before('<tr><td><input type="button" class="remove" value="X"></td><td><input type="button" class="preview" value="Preview" /></td><td><input value="" name="title" type="text"></td><td><input value="" name="link" type="text"></td><td><input value="" name="description" type="text"></td><td><input value="" name="image" type="text"></td></tr>')
        Form.initRemove();
        Form.initPreview();
    }
}

$.fn.serializeObject = function(){

    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push":     /^$/,
            "fixed":    /^\d+$/,
            "named":    /^[a-zA-Z0-9_]+$/
        };


    this.build = function(base, key, value){
        base[key] = value;
        return base;
    };

    this.push_counter = function(key){
        if(push_counters[key] === undefined){
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(this).serializeArray(), function(){

        // skip invalid keys
        if(!patterns.validate.test(this.name)){
            return;
        }

        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

        while((k = keys.pop()) !== undefined){

            // adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

            // push
            if(k.match(patterns.push)){
                merge = self.build([], self.push_counter(reverse_key), merge);
            }

            // fixed
            else if(k.match(patterns.fixed)){
                merge = self.build([], k, merge);
            }

            // named
            else if(k.match(patterns.named)){
                merge = self.build({}, k, merge);
            }
        }

        json = $.extend(true, json, merge);
    });

    return json;
};