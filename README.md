
# Servo.ai

# Installation
1. on linux, sudo apt-get update
2. install nodejs (v8 and above) and npm (v5.5 and above)
3. get from git:
    * npm install -g gulp
    * npm install -g bower
    * npm install --global --production windows-build-tools /  sudo apt-get install build-essential libssl-dev
    * cd ~
	* git clone https://<username>@bitbucket.org/servoai/servo-admin.git
    * cd ~/servo-admin/src/server
    * npm install
    * cd ../client/b3
    * npm install
    * bower install

4. get behavior tree sub-module
* cd ~/servo-admin/src/server
* git submodule update --init --recursive

# Run Servo
We use two terminals, one for the server and one for the editor
On terminal 1:
* cd ~/servo-admin/src/server
* node app.js

On terminal 2:

* cd ~/servo-admin/src/client/b3

* gulp serve

On the browser:

* open localhost:8000

# Build documentation
* cd src/server
* npm run doc
* npm run cpdoc


# Optional/advanced installations:	
	
## **Database:**

### couchbase
 * install couchbase
 * restore from release-proc/couchbase-buckets
 * change db entry at src/server/config.json to 'couchdb'
### mongodb
* install mongodb
 * change db entry at src/server/config.json to 'mongo'


## **Certificates:**
 
1. get a new domain (ie chatflows.online)

2. Install a certificate

* all certificates should be put under a server/certificates/<domain> folder, with following names:
**cert.pem
**chain.pem
**privkey.pem

* change entries at server/config.json:
``"serverBaseDomain": "<domain>",
  "openSSL": true,
``  

## E2E/Unit tests:
run npm test

#License and copyright
* Server is dual-licensed under AGPL and Servo License 
* Client is licensed under MIT license

  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Copyright (c) 2017 Servo Labs Inc
Some of the source files have parts Copyright by Renato de Pontes Pereira

