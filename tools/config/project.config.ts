import { join } from 'path';

import { SeedConfig } from './seed.config';
import { InjectableDependency } from './seed.config.interfaces';

/**
 * This class extends the basic seed configuration, allowing for project specific overrides. A few examples can be found
 * below.
 */
export class ProjectConfig extends SeedConfig {

    PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');


    FONTS_DEST = `${this.APP_DEST}/fonts`;
    FONTS_SRC = [
        'node_modules/bootstrap/dist/fonts/**',
        'node_modules/font-awesome/fonts/**',

    ];
    constructor() {
    super();
    // this.APP_TITLE = 'Put name of your app here';
    let additional_deps: InjectableDependency[] = [
       {src: 'jquery/dist/jquery.min.js', inject: 'libs'},
       {src: 'lodash/index.js', inject: 'libs'},

        {src: 'bootstrap/dist/js/bootstrap.min.js', inject: 'libs'},
        {src: 'backbone/backbone.js', inject: 'libs'},
        {src: 'jointjs/dist/joint.js',inject: 'libs'},

        {src: 'marked/marked.min.js', inject: 'libs'},
        {src: 'ace-builds/src-min-noconflict/ace.js', inject: 'libs'},
        {src: 'ace-builds/src-min-noconflict/theme-monokai.js', inject: 'libs'},
        {src: 'bootstrap/dist/css/bootstrap.min.css', inject: true}, // inject into css section
        {src: 'font-awesome/css/font-awesome.min.css',inject:true},
        {src: 'ag-grid/dist/styles/ag-grid.css', inject: true},
        {src: 'ag-grid/dist/styles/theme-fresh.css', inject: true},
	    {src: 'jointjs/dist/joint.css', inject: true}



    ];

    const seedDependencies = this.NPM_DEPENDENCIES;

    this.NPM_DEPENDENCIES = seedDependencies.concat(additional_deps);

    /* Add to or override NPM module configurations: */
    //this.mergeObject( this.PLUGIN_CONFIGS['browser-sync'], { ghostMode: false } );

    }
}
