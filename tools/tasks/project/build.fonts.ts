import * as gulp from 'gulp';
import {FONTS_SRC, FONTS_DEST} from '../../config';

export = () => {
    console.log('FONTS_SRC',FONTS_SRC);
    console.log('FONTS_DEST',FONTS_DEST);
    return gulp.src(FONTS_SRC)
        .pipe(gulp.dest(FONTS_DEST));
};